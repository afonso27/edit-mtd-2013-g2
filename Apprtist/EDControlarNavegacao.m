//
//  EDControlarNavegacao.m
//  Apprtist
//
//  Created by Jorge Oliveira on 02/05/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//

#import "EDControlarNavegacao.h"
#import "ECSlidingViewController.h"
#import "EDMenuViewController.h"
#import "EDEdicaoViewController.h"
#import "EDDashboardViewController.h"


@interface EDControlarNavegacao ()

@end

@implementation EDControlarNavegacao

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    // shadowPath, shadowOffset, and rotation is handled by ECSlidingViewController.
    // You just need to set the opacity, radius, and color.
    self.view.layer.shadowOpacity = 0.75f;
    self.view.layer.shadowRadius = 10.0f;
    self.view.layer.shadowColor = [UIColor blackColor].CGColor;
    
    if (![self.slidingViewController.underLeftViewController isKindOfClass:[EDMenuViewController class]]) {
        self.slidingViewController.underLeftViewController  = [self.storyboard instantiateViewControllerWithIdentifier:@"Menu"];
    }
    
    //comando para impedir que o slide da direita funcione para esta view e funcione apenas para a Edição.
    self.slidingViewController.underRightViewController = nil;

    //fazer aparecer o botão de menu na barra de navegação e teve que ser custom para desaparecer a borda e formato de botão típico
    // Nota, Joao o butao do menu tem de estar em todos os controllers acho eu, amanha pergunto


    //[self.view addGestureRecognizer:self.slidingViewController.panGesture];

    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    


}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)revealMenu:(id)sender
{
    [self.slidingViewController anchorTopViewTo:ECRight];

}

@end
