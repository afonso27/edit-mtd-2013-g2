//
//  EDAppDelegate.h
//  Apprtist
//
//  Created by Grupo 2 on 24/04/13.
//  Copyright (c) 2013 Grupo 2. All rights reserved.
//

#import <UIKit/UIKit.h>

#define APPDELEGATE ((EDAppDelegate *)[[UIApplication sharedApplication] delegate])
#define CACHE(c) [[APPDELEGATE cachedArt] objectForKey:c]

@interface EDAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property(strong,nonatomic) NSMutableArray * artFiles;
@property(strong,nonatomic) NSArray * categorias;
@property(strong,nonatomic) NSArray * categoriasNames;
@property(strong,nonatomic) NSMutableArray * obras;
@property(strong,nonatomic) NSMutableArray * obrasArtista;
@property(strong,nonatomic) NSMutableDictionary * arraysPorArtista;

@property(strong,nonatomic) NSMutableDictionary * imagensPorCategoria;
@property(strong,nonatomic) NSMutableDictionary * imagensPorArtista;

@property(strong,nonatomic) NSMutableArray * pessoas;
@property(strong,nonatomic) NSMutableArray * cidade;
@property(strong,nonatomic) NSMutableArray * natureza;
@property(strong,nonatomic) NSMutableArray * artistas;

@property(strong,nonatomic) NSMutableDictionary * myDict;

//Dicionário para a Cache
@property(strong,nonatomic) NSMutableDictionary * cachedArt;


@end
