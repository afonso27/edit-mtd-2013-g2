//
//  EDBaseResponse.h
//  Apprtist
//
//  Created by Jorge Oliveira on 26/05/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EDBaseResponse : NSObject

@property (nonatomic, strong) NSNumber *status;
@property (nonatomic, strong) NSString *message;
@property (nonatomic, strong) id data;

+ (EDBaseResponse*) parseResponseFromDictionary:(NSDictionary*)dictionary;

@end
