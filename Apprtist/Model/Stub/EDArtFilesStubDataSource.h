//
//  EDArtFilesStubDataSource.h
//  Apprtist
//
//  Created by Jorge Oliveira on 26/05/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EDArtFilesDataSourceManager.h"

@interface EDArtFilesStubDataSource : NSObject<EDArtFilesDataSourceManager>

//-(void)plistReader;


@end
