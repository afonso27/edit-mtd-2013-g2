//
//  EDArtFilesStubDataSource.m
//  Apprtist
//
//  Created by Jorge Oliveira on 26/05/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//

#import "EDArtFilesStubDataSource.h"
#import "EDArtFilesManager.h"
#import "EDArtFiles.h"
#import "EDBaseResponse.h"

#define kEDArtworkManager_StubArtworksPLIST @"ObrasStub.plist"


@implementation EDArtFilesStubDataSource

- (void)artworkManager:(EDArtFilesManager *)artworkManager
getAllArtworksForUserId:(NSString *)userId
   withCompletionBlock:(void (^)(NSError *, EDBaseResponse *))completionBlock
{
	NSError* error = nil;
	
	NSString *finalPath = [[NSBundle mainBundle] pathForResource:kEDArtworkManager_StubArtworksPLIST
                                                          ofType:nil];
	
	NSDictionary *artworksStub = [NSDictionary dictionaryWithContentsOfFile:finalPath];
    
	EDBaseResponse *realArtworks = [EDBaseResponse parseResponseFromDictionary:artworksStub];
    
	realArtworks.data = [EDArtFiles parseObjectsFromDictionariesArray:realArtworks.data];
    
    NSMutableArray * userWorks = [NSMutableArray new];
    
    
    for (EDArtFiles * file in realArtworks.data) {
        if ([userId isEqualToString: file.artAuthorID]) {
            
            [userWorks addObject:file];
        }
        
    }
    
    realArtworks.data = userWorks;

	completionBlock(error,realArtworks);
}

- (void) artworkManager:(EDArtFilesManager *)artworkManager
getAllArtworksForCategory:(NSString*)category
    withCompletionBlock:(void(^)(NSError * error, EDBaseResponse* response))completionBlock
{
    NSError* error = nil;
	
	NSString *finalPath = [[NSBundle mainBundle] pathForResource:kEDArtworkManager_StubArtworksPLIST
                                                          ofType:nil];
	
	NSDictionary *artworksStub = [NSDictionary dictionaryWithContentsOfFile:finalPath];
    
	EDBaseResponse *realCategoryArtworks = [EDBaseResponse parseResponseFromDictionary:artworksStub];
    
	realCategoryArtworks.data = [EDArtFiles parseObjectsFromDictionariesArray:realCategoryArtworks.data];
    
    NSMutableArray * categoryWorks = [NSMutableArray new];
    for (EDArtFiles * file in realCategoryArtworks.data) {
        if ([category isEqualToString: file.artCategory]) {
            
            [categoryWorks addObject:file];
        }
    }
    
    realCategoryArtworks.data = categoryWorks;
    
	completionBlock(error,realCategoryArtworks);
}

- (void) artworkManager:(EDArtFilesManager *)artworkManager
getAllArtworksForArtID:(NSString*)artID
    withCompletionBlock:(void(^)(NSError * error, EDBaseResponse* response))completionBlock
{
    NSError* error = nil;
	
	NSString *finalPath = [[NSBundle mainBundle] pathForResource:kEDArtworkManager_StubArtworksPLIST
                                                          ofType:nil];
	
	NSDictionary *artworksStub = [NSDictionary dictionaryWithContentsOfFile:finalPath];
    
	EDBaseResponse *realCategoryArtworks = [EDBaseResponse parseResponseFromDictionary:artworksStub];
    
	realCategoryArtworks.data = [EDArtFiles parseObjectsFromDictionariesArray:realCategoryArtworks.data];
    
    NSMutableArray * artIDWorks = [NSMutableArray new];
    
    for (EDArtFiles * file in realCategoryArtworks.data) {
        if ([artID isEqualToString: file.artID]) {
            
            [artIDWorks addObject:file];
        }
    }
    
    realCategoryArtworks.data = artIDWorks;
    
	completionBlock(error,realCategoryArtworks);
}

- (void) artworkManager:(EDArtFilesManager *)artworkManager
 getAllArtworks:(NSString*)artFiles
    withCompletionBlock:(void(^)(NSError * error, EDBaseResponse* response))completionBlock
{
    NSError* error = nil;
	
	NSString *finalPath = [[NSBundle mainBundle] pathForResource:kEDArtworkManager_StubArtworksPLIST
                                                          ofType:nil];
	
	NSDictionary *artworksStub = [NSDictionary dictionaryWithContentsOfFile:finalPath];
    
	EDBaseResponse *realCategoryArtworks = [EDBaseResponse parseResponseFromDictionary:artworksStub];
    
	realCategoryArtworks.data = [EDArtFiles parseObjectsFromDictionariesArray:realCategoryArtworks.data];
    
    NSMutableArray * allArtworks = [NSMutableArray new];
    
    for (EDArtFiles * file in realCategoryArtworks.data) {
    
        [allArtworks addObject:file];
    }
    
    realCategoryArtworks.data = allArtworks;
    
	completionBlock(error,realCategoryArtworks);
}



//-(void)plistReader
//{
////    NSError * error = nil;
//    
//    NSString *finalPath = [[NSBundle mainBundle] pathForResource:kEDArtworkManager_StubArtworksPLIST
//                                                                                        ofType:nil];
//    NSDictionary *artworksStub = [NSDictionary dictionaryWithContentsOfFile:finalPath];
//   
//    BaseResponse *realArtworks = [BaseResponse parseResponseFromDictionary:artworksStub];
//    realArtworks.data = [ArtFiles parseObjectsFromDictionariesArray:realArtworks.data];
//    
////    completionBlock(error,realArtworks);
//    
//}
@end
