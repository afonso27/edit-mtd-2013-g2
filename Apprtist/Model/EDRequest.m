//
//  EDRequest.m
//  Apprtist
//
//  Created by Afonso Neto on 6/25/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//

#import "EDRequest.h"
#import "CJSONDeserializer.h"
#import <Parse/Parse.h>

#define kEDApiEndPointUrl @"http://edit-mtd-2013-1s.blissapplications.com/api/"
#define	kEDContentType_MultipartFormDataKey @"Content-Type"
#define kEDMultipartFormDataBoundary @"WebKitFormBoundaryEG0qL3iLd2XGGv3J"
#define kEDContentType_MultipartFormDataValue @"multipart/form-data; boundary=WebKitFormBoundaryEG0qL3iLd2XGGv3J"

#define kEDWorkPostTitleKey @"Title"
#define kEDWorkPostTagsKey @"Tags"
#define kEDWorkPostLatitudeKey @"Latitude"
#define kEDWorkPostLongitudeKey @"Longitude"
#define kEDWorkPostFileKey @"File"


@implementation EDRequest

- (id)init:(NSURLRequest *)request
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

#pragma mark - RequestMethods

#pragma mark RequestAuthorsMethods
- (NSURLRequest *)RequestAuthors {
    
    if ([PFUser currentUser] && [PFFacebookUtils isLinkedWithUser:[PFUser currentUser]]) {
        // O user esta logado por isso pode se mandar o request
        FBAccessTokenData *tokenData = [[PFFacebookUtils session] accessTokenData];
        // NSLog(@"Accesstoken %@", tokenData.accessToken);
        NSMutableURLRequest *mutableRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[kEDApiEndPointUrl stringByAppendingString:@"authors/"]]];
        [mutableRequest addValue:tokenData.accessToken forHTTPHeaderField:@"FBAccessToken"];
        return (NSURLRequest *)mutableRequest;
    } else {
        NSLog(@"error user nao esta logado");
        return 0;
    }
}

- (NSURLRequest *)RequestAuthorsById:(NSString *)authorId {
    
    if ([PFUser currentUser] && [PFFacebookUtils isLinkedWithUser:[PFUser currentUser]]) {
        // O user esta logado por isso pode se mandar o request
        FBAccessTokenData *tokenData = [[PFFacebookUtils session] accessTokenData];
        // NSLog(@"Accesstoken %@", tokenData.accessToken);
        NSMutableURLRequest *mutableRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[kEDApiEndPointUrl stringByAppendingString:[@"authors/" stringByAppendingString:authorId]]]];
        [mutableRequest addValue:tokenData.accessToken forHTTPHeaderField:@"FBAccessToken"];
        [mutableRequest addValue:authorId forHTTPHeaderField:@"AuthorId"];
        return (NSURLRequest *)mutableRequest;
    } else {
        NSLog(@"error user nao esta logado");

        return 0;
    }
}

#pragma mark RequestTagsMethods

- (NSURLRequest *)RequestTags {
    
    if ([PFUser currentUser] && [PFFacebookUtils isLinkedWithUser:[PFUser currentUser]]) {
        // O user esta logado por isso pode se mandar o request
        FBAccessTokenData *tokenData = [[PFFacebookUtils session] accessTokenData];
        //NSLog(@"Accesstoken %@", tokenData.accessToken);
        NSMutableURLRequest *mutableRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[kEDApiEndPointUrl stringByAppendingString:@"tags/"]]];
        [mutableRequest addValue:tokenData.accessToken forHTTPHeaderField:@"FBAccessToken"];
        return (NSURLRequest *)mutableRequest;
    } else {
        NSLog(@"error user nao esta logado");


        return 0;
    }
}

- (NSURLRequest *)RequestTagsById:(NSString *)tagId {
    
    if ([PFUser currentUser] && [PFFacebookUtils isLinkedWithUser:[PFUser currentUser]]) {
        // O user esta logado por isso pode se mandar o request
        FBAccessTokenData *tokenData = [[PFFacebookUtils session] accessTokenData];
        // NSLog(@"Accesstoken %@", tokenData.accessToken);
        NSMutableURLRequest *mutableRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[kEDApiEndPointUrl stringByAppendingString:[@"tags/" stringByAppendingString:tagId]]]];
        [mutableRequest addValue:tokenData.accessToken forHTTPHeaderField:@"FBAccessToken"];
        [mutableRequest addValue:tagId forHTTPHeaderField:@"TagId"];
        return (NSURLRequest *)mutableRequest;
    } else {
        NSLog(@"error user nao esta logado");


        return 0;
    }
}

#pragma mark RequestWorksMethods

- (NSURLRequest *)RequestAllWorks {
    
    if ([PFUser currentUser] && [PFFacebookUtils isLinkedWithUser:[PFUser currentUser]]) {
        // O user esta logado por isso pode se mandar o request
        FBAccessTokenData *tokenData = [[PFFacebookUtils session] accessTokenData];
        //NSLog(@"Accesstoken %@", tokenData.accessToken);
        NSMutableURLRequest *mutableRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[kEDApiEndPointUrl stringByAppendingString:@"works/all"]]];
        [mutableRequest addValue:tokenData.accessToken forHTTPHeaderField:@"FBAccessToken"];
        return (NSURLRequest *)mutableRequest;
    } else {
        NSLog(@"error user nao esta logado");

        return 0;
    }
    
}

- (NSURLRequest *)RequestWorksById:(NSString *)workId {
    
    if ([PFUser currentUser] && [PFFacebookUtils isLinkedWithUser:[PFUser currentUser]]) {
        // O user esta logado por isso pode se mandar o request
        FBAccessTokenData *tokenData = [[PFFacebookUtils session] accessTokenData];
        // NSLog(@"Accesstoken %@", tokenData.accessToken);
        NSMutableURLRequest *mutableRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[kEDApiEndPointUrl stringByAppendingString:[@"works/" stringByAppendingString:workId]]]];
        [mutableRequest addValue:tokenData.accessToken forHTTPHeaderField:@"FBAccessToken"];
        [mutableRequest addValue:workId forHTTPHeaderField:@"WorkId"];
        return (NSURLRequest *)mutableRequest;
    } else {
        NSLog(@"error user nao esta logado");

        return 0;
    }
}

- (NSURLRequest *)RequestWorksByWorkIds:(NSArray *)worksIds {
    
    if ([PFUser currentUser] && [PFFacebookUtils isLinkedWithUser:[PFUser currentUser]]) {
        
        if (worksIds == NULL) {
            NSLog(@"erro nao pode ser null");
            return 0;
        }
        // O user esta logado por isso pode se mandar o request
        FBAccessTokenData *tokenData = [[PFFacebookUtils session] accessTokenData];
      //  NSLog(@"Accesstoken %@", tokenData.accessToken);
        NSMutableString *workIdsString = [[NSMutableString alloc] init];
        for (int i = 0; i < [worksIds count]; i++) {
            if (i == 0) {
                [workIdsString appendString:[@"?listOfIds=" stringByAppendingString:[NSString stringWithFormat:@"%@",[worksIds objectAtIndex:0]]]];
            } else {
                [workIdsString appendString:[@"&listOfIds=" stringByAppendingString:[NSString stringWithFormat:@"%@",[worksIds objectAtIndex:i]]]];
            }
        }
        NSMutableURLRequest *mutableRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[kEDApiEndPointUrl stringByAppendingString:[@"works/byids" stringByAppendingString:workIdsString]]]];
        //NSLog(@"%@",[kEDApiEndPointUrl stringByAppendingString:[@"works/byids" stringByAppendingString:workIdsString]]);
        [mutableRequest addValue:tokenData.accessToken forHTTPHeaderField:@"FBAccessToken"];
        return (NSURLRequest *)mutableRequest;
    } else {
        NSLog(@"error user nao esta logado");

        return 0;
    }
}

- (NSURLRequest *) RequestToPublishWork:(NSString *)ficheiro withName:(NSString *)nomeImage andTags:(NSArray *)tagArray {
    
    if ([PFUser currentUser] && [PFFacebookUtils isLinkedWithUser:[PFUser currentUser]]) {
        NSLog(@"%@", ficheiro);
        FBAccessTokenData *tokenData = [[PFFacebookUtils session] accessTokenData];
        //NSLog(@"Accesstoken %@", tokenData.accessToken);
        NSMutableURLRequest *mutableRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[kEDApiEndPointUrl stringByAppendingString:@"works/"]]];
        mutableRequest.HTTPMethod = @"POST";
        [mutableRequest addValue:tokenData.accessToken forHTTPHeaderField:@"FBAccessToken"];
        [mutableRequest addValue:kEDContentType_MultipartFormDataValue forHTTPHeaderField:kEDContentType_MultipartFormDataKey];
        CLLocationCoordinate2D coordinate = [self getLocation];
        NSArray *tagsArray = tagArray; // tags
        NSString *filePath = ficheiro; // ficheiro para publicar
        NSString *title = nomeImage; // titulo da foto
        NSNumber *latitude = [NSNumber numberWithDouble:coordinate.latitude]; // latitude
        NSNumber *longitude = [NSNumber numberWithDouble:coordinate.longitude]; // longitude
        NSString *artFile = filePath;
        // adicionar uns labels para as coordenadas da img
        NSLog(@"*dLatitude : %f", coordinate.latitude);
        NSLog(@"*dLongitude : %f",coordinate.longitude);
        
        
        
        
        
        NSData *imageData = [NSData dataWithContentsOfFile:filePath];
        
        // cria dicionario de parametros necessarios
        
        NSMutableDictionary *paramDict = [NSMutableDictionary dictionary];
        [paramDict setValue:title forKey:kEDWorkPostTitleKey];
        [paramDict setValue:latitude forKey:kEDWorkPostLatitudeKey];
        [paramDict setValue:longitude forKey:kEDWorkPostLongitudeKey];
        [paramDict setValue:tagsArray forKey:kEDWorkPostTagsKey];
        [paramDict setValue:imageData forKey:kEDWorkPostFileKey];
        
        NSMutableData *bodyData = [NSMutableData data];
        
        for (NSString *key in paramDict.allKeys)
        {
            id value = [paramDict valueForKey:key];
            
            if([value isKindOfClass:[NSData class]])
            {
                NSData *valueData = (NSData*)value;
                NSMutableString *paramValue = [NSMutableString stringWithFormat:@"--%@\r\n",kEDMultipartFormDataBoundary];
                [paramValue appendFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n",key,artFile];
                [paramValue appendFormat:@"Content-Type: image/jpeg\r\n\r\n"];
                
                NSMutableData *paramData = [NSMutableData dataWithData:[paramValue dataUsingEncoding:NSUTF8StringEncoding]];
                [paramData appendData:valueData];
                [paramData appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
                [bodyData appendData:paramData];
            }
            else if([value isKindOfClass:[NSArray class]])
            {
                NSArray *valueArray = (NSArray*)value;
                NSString *valueString = [valueArray componentsJoinedByString:@","];
                NSMutableString *paramValue = [NSMutableString stringWithFormat:@"--%@\r\n",kEDMultipartFormDataBoundary];
                [paramValue appendFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",key];
                [paramValue appendFormat:@"%@\r\n",valueString];
                NSData *paramData = [paramValue dataUsingEncoding:NSUTF8StringEncoding];
                [bodyData appendData:paramData];
            }
            else if([value isKindOfClass:[NSNumber class]])
            {
                NSNumber *valueNumber = (NSNumber*)value;
                NSString *valueString = [valueNumber stringValue];
                NSMutableString *paramValue = [NSMutableString stringWithFormat:@"--%@\r\n",kEDMultipartFormDataBoundary];
                [paramValue appendFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",key];
                [paramValue appendFormat:@"%@\r\n",valueString];
                NSData *paramData = [paramValue dataUsingEncoding:NSUTF8StringEncoding];
                [bodyData appendData:paramData];
            }
            else
            {
                NSMutableString *paramValue = [NSMutableString stringWithFormat:@"--%@\r\n",kEDMultipartFormDataBoundary];
                [paramValue appendFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",key];
                [paramValue appendFormat:@"%@\r\n",value];
                NSData *paramData = [paramValue dataUsingEncoding:NSUTF8StringEncoding];
                [bodyData appendData:paramData];
            }
        }
        NSData *boundary = [[NSString stringWithFormat:@"--%@--\r\n",kEDMultipartFormDataBoundary] dataUsingEncoding:NSUTF8StringEncoding];
        
        [bodyData appendData:boundary];
        mutableRequest.HTTPBody = bodyData;
        
        
        return (NSURLRequest *)mutableRequest;
    }else {
        
        NSLog(@"error user nao esta logado");

        return 0;
    }
    
}

#pragma mark RequestAuth

- (NSURLRequest *)RequestAuth {
    
    if ([PFUser currentUser] && [PFFacebookUtils isLinkedWithUser:[PFUser currentUser]]) {
        // O user esta logado por isso pode se mandar o request
        FBAccessTokenData *tokenData = [[PFFacebookUtils session] accessTokenData];
        NSString *FbId = [[PFUser currentUser] objectForKey:@"fbId"];
        // NSLog(@"Accesstoken %@", tokenData.accessToken);
        NSMutableURLRequest *mutableRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[kEDApiEndPointUrl stringByAppendingString:@"auth"]]];
        mutableRequest.HTTPMethod = @"POST";
        [mutableRequest addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        NSString *AuthenticationRequestDataString = [NSString stringWithFormat:@"{\"FBAccessToken\":\"%@\",\n\"FBUserId\":\"%@\"}",tokenData.accessToken,FbId];
        NSData *AuthenticationRequestData = [AuthenticationRequestDataString dataUsingEncoding:NSUTF8StringEncoding];
        mutableRequest.HTTPBody = AuthenticationRequestData;
        return (NSURLRequest *)mutableRequest;
    } else {
        NSLog(@"error user nao esta logado");

        return 0;
    }
}

#pragma mark -


-(void)fetchData:(NSData *)data{
    self.dados = [[NSDictionary alloc] init];
    NSError *JsonParseError;
    NSDictionary *json = [[CJSONDeserializer deserializer] deserializeAsDictionary:data error:&JsonParseError];
    //NSLog(@"%@",json);
    if (JsonParseError) {
        NSLog(@"%@",JsonParseError);
    } else {
        self.dados = json;
    }
}


#pragma mark - NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    // A response has been received, this is where we initialize the instance var you created
    // so that we can append data to it in the didReceiveData method
    // Furthermore, this method is called each time there is a redirect so reinitializing it
    // also serves to clear it
    _responseData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    // Append the new data to the instance variable you declared
    [_responseData appendData:data];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    // The request is complete and data has been received
    // You can parse the stuff in your instance variable now
    // self.dados = _responseData;
    [self fetchData:_responseData];
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // The request has failed for some reason!
    // Check the error var
}

#pragma mark - getCurrentLocation

-(CLLocationCoordinate2D) getLocation{
    CLLocationManager *locationManager = [[CLLocationManager alloc] init] ;
    locationManager.delegate = nil;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    [locationManager startUpdatingLocation];
    CLLocation *location = [locationManager location];
    CLLocationCoordinate2D coordinate = [location coordinate];
    
    return coordinate;
}

@end
