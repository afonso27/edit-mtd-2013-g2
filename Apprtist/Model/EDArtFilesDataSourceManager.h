//
//  EDArtFilesDataSourceManager.h
//  Apprtist
//
//  Created by Jorge Oliveira on 26/05/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//

#import <Foundation/Foundation.h>

@class EDBaseResponse;
@class EDArtFilesManager;

@protocol EDArtFilesDataSourceManager <NSObject>

- (void) artworkManager:(EDArtFilesManager *)artworkManager
getAllArtworksForUserId:(NSString*)userId
    withCompletionBlock:(void(^)(NSError * error, EDBaseResponse* response))completionBlock;

- (void) artworkManager:(EDArtFilesManager *)artworkManager
getAllArtworksForCategory:(NSString*)category
    withCompletionBlock:(void(^)(NSError * error, EDBaseResponse* response))completionBlock;

- (void) artworkManager:(EDArtFilesManager *)artworkManager
getAllArtworksForArtID:(NSString*)artID
    withCompletionBlock:(void(^)(NSError * error, EDBaseResponse* response))completionBlock;

- (void) artworkManager:(EDArtFilesManager *)artworkManager
 getAllArtworks:(NSNumber*)artFiles
    withCompletionBlock:(void(^)(NSError * error, EDBaseResponse* response))completionBlock;

//- (void) artworkManager:(EDArtFilesManager *)artworkManager
//getAllTagsWithCompletionBlock:(void(^)(NSError * error, EDBaseResponse* response))completionBlock;

@end
