//
//  EDArtistas.h
//  Apprtist
//
//  Created by Jorge Oliveira on 22/05/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EDArtFiles.h"

@class EDArtFiles;

@interface EDArtistas : NSObject

//@property(strong,nonatomic) NSString * artist;
//@property(strong,nonatomic) NSMutableArray * obras;

@property(strong,nonatomic) NSNumber * userId;
@property(strong,nonatomic) NSString * fbID;
@property(strong,nonatomic) NSString * userName;


@end
