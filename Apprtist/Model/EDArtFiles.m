//
//  EDArtFiles.m
//  Apprtist
//
//  Created by Jorge Oliveira on 20/05/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//

#import "EDArtFiles.h"

@implementation EDArtFiles



#define kEDArtwork_DictionaryKey_artID @"artID"
#define kEDArtwork_DictionaryKey_artName @"artName"
#define kEDArtwork_DictionaryKey_artDate @"artDate"
#define kEDArtwork_DictionaryKey_artAuthorID @"artAuthorID"
#define kEDArtwork_DictionaryKey_artLatitude @"artLatitude"
#define kEDArtwork_DictionaryKey_artLongitude @"artLongitude"
#define kEDArtwork_DictionaryKey_artCategory @"artCategory"
#define kEDArtwork_DictionaryKey_artFileURLLocation @"artFileURLLocation"
#define kEDArtwork_DictionaryKey_artDescription @"artDescription"
#define kEDArtwork_DictionaryKey_artStatus @"artStatus"


+ (NSArray*) parseObjectsFromDictionariesArray:(NSArray*)dictionariesArray
{
    NSMutableArray *resultingArray = [NSMutableArray arrayWithCapacity:dictionariesArray.count];
	
	for (NSDictionary *dictionary in dictionariesArray) {
        [resultingArray addObject:[self parseObjectFromDictionary:dictionary]];
	}
	return resultingArray;
}

+ (EDArtFiles*) parseObjectFromDictionary:(NSDictionary*)dictionary
{
    EDArtFiles *artwork = [[EDArtFiles alloc]init];
    artwork.artID = [dictionary objectForKey:kEDArtwork_DictionaryKey_artID];
	artwork.artName = [dictionary objectForKey:kEDArtwork_DictionaryKey_artName];
    artwork.artDate = [dictionary objectForKey:kEDArtwork_DictionaryKey_artDate];
    artwork.artLatitude = [dictionary objectForKey:kEDArtwork_DictionaryKey_artLatitude];
    artwork.artLongitude = [dictionary objectForKey:kEDArtwork_DictionaryKey_artLongitude];
    artwork.artCategory = [dictionary objectForKey:kEDArtwork_DictionaryKey_artCategory];
    artwork.artFileURLLocation = [dictionary objectForKey:kEDArtwork_DictionaryKey_artFileURLLocation];
    artwork.artAuthorID = [dictionary objectForKey:kEDArtwork_DictionaryKey_artAuthorID];
    
	return artwork;
}


@end
