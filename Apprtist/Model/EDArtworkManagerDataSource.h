//
//  EDArtworkManagerDataSource.h
//  EDITStub
//
//  Created by Tiago Janela on 20/05/13.
//  Copyright (c) 2013 Bliss Applications. All rights reserved.
//

#import <Foundation/Foundation.h>

@class EDBaseResponse;
@class EDArtFilesManager;

@protocol EDArtworkManagerDataSource <NSObject>

- (void) artworkManager:(EDArtFilesManager *)artworkManager
getAllArtworksForUserId:(NSString*)userId
			withProgressBlock:(void(^)(CGFloat progress))progressBlock
		withCompletionBlock:(void(^)(NSError* error, EDBaseResponse* response))completionBlock;



@end
