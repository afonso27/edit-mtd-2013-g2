
//
//  main.m
//  Apprtist
//
//  Created by Grupo 2 on 24/04/13.
//  Copyright (c) 2013 Grupo 2. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "EDAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([EDAppDelegate class]));
    }
}
