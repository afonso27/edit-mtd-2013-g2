//
//  EDCameraViewController.h
//  Apprtist
//
//  Created by Jorge Oliveira on 29/04/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EDCameraViewController : UIViewController <UIApplicationDelegate,UIPickerViewDelegate>

- (UIImagePickerController*) getRollAlbum;
- (UIImagePickerController*) getCameraSource;
@end
