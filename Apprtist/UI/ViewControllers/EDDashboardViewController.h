//
//  EDDashboardViewController.h
//  Apprtist
//
//  Created by Jorge Oliveira on 12/09/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EDDashboardViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIView *loggedIn;
@property (strong, nonatomic) IBOutlet UILabel *facebookName;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (strong, nonatomic) IBOutlet UIView *facebookView;
@property (strong, nonatomic) IBOutlet UILabel *welcomeLbl;
@property (strong, nonatomic) IBOutlet UIButton *logoutBtn;
@property (strong, nonatomic) IBOutlet UIView *logoutBtnView;


- (IBAction)edtiBtnPressed:(id)sender;
- (IBAction)facebookLoginPressed:(id)sender;
- (IBAction)mapsBtnPressed:(id)sender;
- (IBAction)latestWorksBtnPressed:(id)sender;
- (IBAction)userBtnPressed:(id)sender;
- (IBAction)communityBtnPressed:(id)sender;
- (IBAction)galleryBtnPressed:(id)sender;
- (IBAction)botaoLogout:(id)sender;

@property (strong, nonatomic) IBOutlet UIBarButtonItem *logoView;
@end
