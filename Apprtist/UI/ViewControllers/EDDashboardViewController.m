//
//  EDDashboardViewController.m
//  Apprtist
//
//  Created by Jorge Oliveira on 12/09/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//


#import <Parse/Parse.h>
#import "EDRequest.h"
#import "EDURLConnectionLoader.h"
#import "CJSONDeserializer.h"
#import "EDReachability.h"
#import "EDDashboardViewController.h"
#import "ECSlidingViewController.h"
#import "EDMenuViewController.h"
#import "EDAppDelegate.h"

@interface EDDashboardViewController ()

@property (strong, nonatomic) UIBarButtonItem *logoutButton;


@end

@implementation EDDashboardViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - View Methods
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    /*_logoutButton = [[UIBarButtonItem alloc] initWithTitle:@"Log Out" style:UIBarButtonItemStyleBordered target:self action:@selector(ButaoLogout:)];
    self.navigationItem.rightBarButtonItem = _logoutButton;*/
    
    [_logoutBtn addTarget:self action:@selector(botaoLogout:) forControlEvents:UIControlEventTouchUpInside];

    
    self.title = @"Home";
    
    EDReachability *puca = [EDReachability reachabilityWithHostname:@"www.google.com"];
    NetworkStatus temNet = [puca currentReachabilityStatus];
    if(temNet == NotReachable) {
        NSLog(@"There's no internet connection at all. Display error message now.");
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Internet Connection Required for\nPublish" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Dismiss", nil];
        [alert show];
    }else if (temNet == ReachableViaWWAN) {
        
        NSLog(@"We have a 3G connection");
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:@"Warning: 3G Connection detected.\nWant to Continue?" delegate:nil cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
        //alert.tag = 3;
        [alert show];
    } else if (temNet == ReachableViaWiFi) {
        
    }
    
    
    if ([PFUser currentUser] && [PFFacebookUtils isLinkedWithUser:[PFUser currentUser]]) {
        
        //[_logoutButton setEnabled:YES];
        //self.loggedIn.hidden = NO;
        //[self updateFBProfile];
        self.logoutBtn.hidden = NO;
        self.logoutBtnView.hidden = NO;
        self.facebookView.hidden = YES;
         self.welcomeLbl.text = [NSString stringWithFormat:@"WELCOME, %@!",[[PFUser currentUser] objectForKey:@"profile"][@"name"]];
        
    } else {
        [PFUser logOut];
        //[_logoutButton setEnabled:NO];
        self.loggedIn.hidden = YES;
        self.logoutBtn.hidden = YES;
        self.logoutBtnView.hidden = YES;

        
        self.facebookView.hidden = NO;
         self.welcomeLbl.text = @"";
    }
    
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - Buttons
#pragma mark Edit
- (IBAction)edtiBtnPressed:(id)sender {
    
    UIViewController *newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Edit"];
    
    self.slidingViewController.topViewController = newTopViewController;
    
}

#pragma mark Login
- (IBAction)facebookLoginPressed:(id)sender {
    
    
    // Butao Login do Facebook/Parser
    // Set permissions required from the facebook user account
    NSArray *permissionsArray = @[ @"user_about_me", @"user_birthday", @"user_location"];
    // Login PFUser using facebook
    [PFFacebookUtils logInWithPermissions:permissionsArray block:^(PFUser *user, NSError *error) {
        [_activityIndicator stopAnimating]; // Hide loading indicator
        EDRequest *pedido = [[EDRequest alloc] init];
        EDURLConnectionLoader *loader = [[EDURLConnectionLoader alloc] initWithRequest:[pedido RequestAuth]];
        loader.progressBlock = 0;
        loader.completionBlock = ^(NSError *error, NSData * responseData){
            NSError *jsonParseError = NULL;
            NSDictionary *json = [[CJSONDeserializer deserializer] deserializeAsDictionary:responseData error:&jsonParseError];
            if (!jsonParseError) {
                NSDictionary *data = [json objectForKey:@"Data"][@"Author"];
                [[PFUser currentUser] setObject:[data objectForKey:@"Id"] forKey:@"id"];
                [[PFUser currentUser] saveInBackground];
            }
        };
        [loader load];
        [self updateFBProfile];
        
        [self facebookLoginSuccessfull];
        
        if (!user) {
            if (!error) {
                NSLog(@"Uh oh. The user cancelled the Facebook login.");
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Log In Error" message:@"Uh oh. The user cancelled the Facebook login." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Dismiss", nil];
                [alert show];
            } else {
                NSLog(@"Uh oh. An error occurred: %@", error);
                //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Log In Error" message:[error description] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Dismiss", nil];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Log In Error" message:@"Sry can't connect to Facebook" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Dismiss", nil];
                [alert show];
            }
            
            self.facebookView.hidden = NO;
            self.logoutBtn.hidden = YES;
            
        } else if (user.isNew) {
            NSLog(@"User with facebook signed up and logged in!");
            [_logoutButton setEnabled:YES];
            //self.loggedIn.hidden = NO;
            [FBRequestConnection startForMeWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                if (!error) {
                    // Store the current user's Facebook ID on the user
                    [[PFUser currentUser] setObject:[result objectForKey:@"id"]
                                             forKey:@"fbId"];
                    [[PFUser currentUser] saveInBackground];
                }
            }];
            
            [self facebookLoginSuccessfull];
            
        } else {
            NSLog(@"User with facebook logged in!");
            [_logoutButton setEnabled:YES];
            //self.loggedIn.hidden = NO;
            [self facebookLoginSuccessfull];

            
        }
    }];
    
    [_activityIndicator startAnimating];    
    
}

-(void)facebookLoginSuccessfull
{

    
    [UIView animateWithDuration:0.5f animations:^{
        
        self.facebookView.hidden = YES;
        self.welcomeLbl.text = [NSString stringWithFormat:@"WELCOME, %@!",[[PFUser currentUser] objectForKey:@"profile"][@"name"]];
        self.welcomeLbl.hidden = NO;
        self.logoutBtn.hidden = NO;
        self.logoutBtnView.hidden = NO;

        
    }];
    [UIView commitAnimations];
}

- (IBAction)botaoLogout:(id)sender {
    [PFUser logOut];
    //[_logoutButton setEnabled:NO];
    /*UIViewController *newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Home"];
    self.slidingViewController.topViewController = newTopViewController;*/
    
    [UIView animateWithDuration:1.0f animations:^{
        
        self.facebookView.hidden = NO;
        self.welcomeLbl.text = @"";
        self.welcomeLbl.hidden = YES;
        self.logoutBtn.hidden = YES;
        self.logoutBtnView.hidden = YES;

        
    }];
    [UIView commitAnimations];
    
}

#pragma mark Maps
- (IBAction)mapsBtnPressed:(id)sender {
    
    if ([PFUser currentUser] && [PFFacebookUtils isLinkedWithUser:[PFUser currentUser]]) {
        
        UIViewController *newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Map"];
        
        self.slidingViewController.topViewController = newTopViewController;
    } else {
        [self userNotLogged];
    }
}

#pragma mark LatestWorks
- (IBAction)latestWorksBtnPressed:(id)sender {
    
    if ([PFUser currentUser] && [PFFacebookUtils isLinkedWithUser:[PFUser currentUser]]) {
        
        UIViewController *newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Latest Works"];
        
        self.slidingViewController.topViewController = newTopViewController;
    } else {
        [self userNotLogged];
    }
}

#pragma mark User
- (IBAction)userBtnPressed:(id)sender {
    
    
    if ([PFUser currentUser] && [PFFacebookUtils isLinkedWithUser:[PFUser currentUser]]) {
        
        UIViewController *newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"User"];
        
        self.slidingViewController.topViewController = newTopViewController;
    } else {
        [self userNotLogged];
    }
}

#pragma mark Community
- (IBAction)communityBtnPressed:(id)sender {
    
    if ([PFUser currentUser] && [PFFacebookUtils isLinkedWithUser:[PFUser currentUser]]) {
        
        UIViewController *newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Community"];
        
        self.slidingViewController.topViewController = newTopViewController;
    } else {
        [self userNotLogged];
    }
}

/*
#pragma mark Help
- (IBAction)helpBtnPressed:(id)sender {
    
    
    UIViewController *newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Help"];
    
    self.slidingViewController.topViewController = newTopViewController;
    
}*/

#pragma mark Gallery
- (IBAction)galleryBtnPressed:(id)sender {
    
    if ([PFUser currentUser] && [PFFacebookUtils isLinkedWithUser:[PFUser currentUser]]) {
        
        
        UIViewController *newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Gallery"];
        
        self.slidingViewController.topViewController = newTopViewController;
    } else {
        [self userNotLogged];
    }
}

#pragma mark - Support Methods
- (IBAction)ButaoLogout:(id)sender {
    
    [PFUser logOut];
    //NSLog(@"User with facebook logged out! - EDUtilizadorViewController");
    [_logoutButton setEnabled:NO];
    self.loggedIn.hidden = YES;
    
}

-(void)updateFBProfile {
    
    if ([[PFUser currentUser] objectForKey:@"profile"][@"name"]) {
        self.facebookName.text = [[PFUser currentUser] objectForKey:@"profile"][@"name"];
    }
}

-(IBAction)revealMenu:(id)sender
{
    [self.slidingViewController anchorTopViewTo:ECRight];
}

-(void)userNotLogged
{
    UIAlertView * av = [[UIAlertView alloc] initWithTitle:@"Facebook login" message:@"In order to access this area you must login with your Facebook account first" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [av show];
}
@end
