//
//  EDPublicarImagemViewController.m
//  Apprtist
//
//  Created by Afonso Neto on 7/3/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//

#import "EDPublicarImagemViewController.h"
#import "EDURLConnectionLoader.h"
#import "EDRequest.h"
#import "CJSONDeserializer.h"
#import "ECSlidingViewController.h"

@interface EDPublicarImagemViewController ()

@property (strong, nonatomic) NSMutableArray *Tags;
@property(strong,nonatomic) NSMutableArray * tagsNames;
@property(strong,nonatomic) NSMutableArray * selectedTagsNames;
@property(strong,nonatomic) NSMutableArray * unselectedTagsNames;
@end

@implementation EDPublicarImagemViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - View
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"texture_grain.png"]];
    
    UIImage *image = [[UIImage alloc] init];
    
    if (self.img) {
        
        image = [UIImage imageWithData:[NSData dataWithContentsOfFile:self.img]];
        
    } else {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        
        NSString *filePath = [NSString stringWithFormat:@"%@/%@",
        documentsDirectory,@"imgPublica.png"];
        
        self.img = filePath;
        
        image = [UIImage imageWithContentsOfFile:filePath];
    }
    

    self.imagem.image = image;
    self.Tags = [NSMutableArray new];
    self.tagsNames = [NSMutableArray new];
    self.imgTitle.delegate = self;
    self.imagem.contentMode = UIViewContentModeScaleAspectFit;
    self.selectedTagsNames = [NSMutableArray new];
    
    self.tagsTitle.hidden = YES;

}

-(void)viewWillAppear:(BOOL)animated {
    
    [self.publicarLabel setHidden:YES];
    [self.progressoYEAH setHidden:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)chooseTagButtonTapped:(id)sender {
    if (self.tagPicker == nil) {
        //Create the ColorPickerViewController.
        self.tagPicker = [[EDTagsPickerTableViewController alloc] init];
        
        //Set this VC as the delegate.
        self.tagPicker.delegate = self;
    }
    if (self.tagPickerPopover == nil) {
        //The color picker popover is not showing. Show it.
        self.tagPickerPopover = [[UIPopoverController alloc] initWithContentViewController:self.tagPicker];
        [self.tagPickerPopover presentPopoverFromRect:CGRectMake(656, 312, 142, 37) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
       // [self.tagPickerPopover presentPopoverFromBarButtonItem:(UIBarButtonItem *)sender
                                    //  permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    } else {
        //The color picker popover is showing. Hide it.
        [self.tagPickerPopover dismissPopoverAnimated:YES];
        self.tagPickerPopover = nil;
    }
   // NSLog(@"%@",self.Tags);
    
}


-(void)DismissedPop:(NSArray *)selectedTags withUnselected:(NSArray *)unselectedTags{
    
    [self.Tags removeAllObjects];
    [self.Tags addObjectsFromArray:selectedTags];
    [self.unselectedTagsNames addObjectsFromArray:unselectedTags];
    
    //Dismiss the popover if it's showing.
    if (self.tagPickerPopover) {
        [self.tagPickerPopover dismissPopoverAnimated:YES];
        self.tagPickerPopover = nil;
    }
    

    
}


-(void)tagsName:(NSArray *)tagsNames
{
    for (UIView * subview in [self.tagsView subviews]) {
        
        if ([subview isKindOfClass:[UILabel class]]) {
            [subview removeFromSuperview];
        }
    }

    [self.tagsNames addObjectsFromArray:tagsNames];
    
    [self.selectedTagsNames removeAllObjects];
    
    for (NSNumber * tagID in self.Tags) {
        
        [self.selectedTagsNames addObject:[self.tagsNames objectAtIndex:[tagID integerValue]-1]];
        
    }
    
    if (self.selectedTagsNames.count == 0) {
        self.tagsTitle.hidden = YES;
    }else{
        self.tagsTitle.hidden = NO;
    }
    
    float xPlace = 0;
    float yPlace = 0;
    float width = 120;
    float height = 30;
    
    if ([self.selectedTagsNames count] < 6) {
        
        for (NSDictionary * tagName in self.selectedTagsNames) {
            
            UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(xPlace, yPlace, width, height)];
            label.text = [tagName objectForKey:@"Nome"];
            label.textColor = [UIColor blackColor];
            label.backgroundColor = [UIColor clearColor];
            [self.tagsView addSubview:label];
            
            yPlace += height;
        }
        
    }else{
        
        for (int i = 0; i < 6; i++) {
            
            NSDictionary * tagName = [self.selectedTagsNames objectAtIndex:i];
            
            UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(xPlace, yPlace, width, height)];
            label.text = [tagName objectForKey:@"Nome"];
            label.textColor = [UIColor blackColor];
            [self.tagsView addSubview:label];
            
            yPlace += height;
        }
        
        float newX = width + 5;
        yPlace = 0;
        
        for (int j = 6; j < [self.selectedTagsNames count]; j++) {
            
            NSDictionary * tagName = [self.selectedTagsNames objectAtIndex:j];
            
            UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(newX, yPlace, width, height)];
            label.text = [tagName objectForKey:@"Nome"];
            label.textColor = [UIColor blackColor];
            [self.tagsView addSubview:label];
            
            yPlace += height;
            
        }
        
        }
    

}

#pragma mark - Pubish Method
- (IBAction)publicaImagem:(id)sender {
    
    [self.progressoYEAH setHidden:NO];
    [self.publicarLabel setHidden:NO];
    if ([self.imgTitle.text isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erro" message:@"Erro: Nome da imagem não pode ser nulo" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [self.publicarLabel setHidden:YES];
        [self.progressoYEAH setHidden:YES];
        [alert show];
    }  else if(self.Tags == nil) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erro" message:@"Erro: Tem de escolher pelo menos uma Tag" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [self.publicarLabel setHidden:YES];
        [self.progressoYEAH setHidden:YES];
        [alert show];
        
    }else{
        
        NSString *nomeDaImagem = self.imgTitle.text;
        EDRequest *pedido = [[EDRequest alloc] init];
        EDURLConnectionLoader *loader = [[EDURLConnectionLoader alloc] initWithRequest:[pedido RequestToPublishWork:self.img withName:nomeDaImagem andTags:self.Tags]];
        loader.progressBlock = ^(CGFloat progress){
           // NSLog(@"Progress of upload: %f %%", progress * 100);
            [self.progressoYEAH setHidden:NO];
            [self.publicarLabel setHidden:NO];
            self.progressoYEAH.progress = progress;
        };
        loader.completionBlock = ^(NSError *error, NSData * responseData){
            NSError *jsonParseError = NULL;
            NSDictionary *json = [[CJSONDeserializer deserializer] deserializeAsDictionary:responseData error:&jsonParseError];
            if (!jsonParseError) {
                NSDictionary *data = [json objectForKey:@"Data"];
                // TODO: processar resposta do server
                NSLog(@"Upload Complete! --- success!");
                NSLog(@"%@",data);
                [self.publicarLabel setHidden:YES];
                [self.progressoYEAH setHidden:YES];
                
                UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Success" message:@"Image uploaded with success!" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                [av show];
                
                [self deleteImageAfterPublished];
                UIViewController *newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Home"];
                self.slidingViewController.topViewController = newTopViewController;
                
                
            } else {
                NSLog(@"Upload Error!");
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Upload Error" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Dismiss", nil];
                [alert show];
            }
        };
        [loader load];
    }
    
}

-(void)deleteImageAfterPublished
{
    
    NSError * error = nil;

    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString * ficheiro = [NSString stringWithFormat:@"%@",self.img];
    
    [fileManager removeItemAtPath:ficheiro error:&error];

}

#pragma mark - TextField Delegate
-(BOOL) textFieldShouldReturn: (UITextField *) textField{
    [textField resignFirstResponder];
    return YES;
}

@end
