//
//  EDArtistaViewController.h
//  Apprtist
//
//  Created by Jorge Oliveira on 29/04/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EDArtistaViewController : UIViewController <UICollectionViewDataSource,UICollectionViewDelegate>


@property(strong,nonatomic) NSMutableArray * userPublishedArtworks;

@property(nonatomic, strong) NSMutableArray *author;

@property (strong, nonatomic) IBOutlet UIImageView *FBProfilePic;
@property (strong, nonatomic) IBOutlet UILabel *FBUserName;
@property (nonatomic, strong) NSMutableData *imageData;
@property(nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property(nonatomic, strong) NSArray *myCurrentArtist;

@end
