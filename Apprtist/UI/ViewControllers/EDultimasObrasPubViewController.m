//
//  EDultimasObrasPubViewController.m
//  Apprtist
//
//  Created by Afonso Neto on 7/3/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//


#import <Parse/Parse.h>
#import "EDultimasObrasPubViewController.h"
#import "EDultimasObrasPubCell.h"
#import "EDMenuViewController.h"
#import "ECSlidingViewController.h"
#import "EDURLConnectionLoader.h"
#import "EDRequest.h"
#import "CJSONDeserializer.h"
#import "EDAppSupportTasks.h"

//#import <SDWebImage/UIImageView+WebCache.h>

@interface EDultimasObrasPubViewController ()

@property(strong,nonatomic) NSMutableArray *workArray;

@end

@implementation EDultimasObrasPubViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - View Methods
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Butão de logout
    /*self.logoutButton = [[UIBarButtonItem alloc] initWithTitle:@"Log Out" style:UIBarButtonItemStyleBordered target:self action:@selector(ButaoLogout:)];
    self.navigationItem.rightBarButtonItem = self.logoutButton;*/
    
    // Butão do menu
    UIButton * button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 38, 30)];
    [button setImage:[UIImage imageNamed:@"menuButton.png"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(revealMenu:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * menu = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = menu;
    
    self.title = @"Latest Works";
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"texture_grain.png"]];
    
    //[self.logoutButton setEnabled:YES];
    
    [_logoutButton addTarget:self action:@selector(ButaoLogout:) forControlEvents:UIControlEventTouchUpInside];
    
    if ([PFUser currentUser] && [PFFacebookUtils isLinkedWithUser:[PFUser currentUser]]) {
        [_logoutButton setEnabled:YES];
    } else {
        [_logoutButton setEnabled:NO];
    }
    
    if ([[PFUser currentUser] objectForKey:@"profile"][@"name"]) {
        self.FBUserName.text = [[PFUser currentUser] objectForKey:@"profile"][@"name"];
    }
    

}

-(void)viewWillAppear:(BOOL)animated {
    
    /*self.activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    self.activity.center = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2);
    self.activity.frame = self.view.frame;
    self.activity.hidesWhenStopped = YES;
    self.activity.backgroundColor = [UIColor blackColor];
    [self.view addSubview:self.activity];
    [_activity startAnimating];*/
    
    
    
    EDRequest *pedido = [[EDRequest alloc] init];
    EDURLConnectionLoader *loader = [[EDURLConnectionLoader alloc] initWithRequest:[pedido RequestAllWorks]] ;
    loader.progressBlock = 0;
	loader.completionBlock = ^(NSError *error, NSData * responseData){
        NSError *jsonParseError = NULL;
        NSDictionary *json = [[CJSONDeserializer deserializer] deserializeAsDictionary:responseData error:&jsonParseError];
        if (!jsonParseError) {
            self.workArray = [[NSMutableArray alloc] init];
            for (NSDictionary *dict in [json objectForKey:@"Data"]) {
                NSMutableDictionary *works = [NSMutableDictionary new];
                [works setObject:[dict objectForKey:@"Title"] forKey:@"Nome"];
                [works setObject:[dict objectForKey:@"File"] forKey:@"Ficheiro"];
                [works setObject:[dict objectForKey:@"Latitude"] forKey:@"Latitude"];
                [works setObject:[dict objectForKey:@"Longitude"] forKey:@"Longitude"];
                [works setObject:[dict objectForKey:@"AuthorFBID"] forKey:@"Autor"];
                [works setObject:[dict objectForKey:@"CreationDate"] forKey:@"data"];
                [self.workArray addObject:works];
            }
            
            [self.collectionView reloadData];
        }
	};
	[loader load];
    
    if ([PFUser currentUser] && [PFFacebookUtils isLinkedWithUser:[PFUser currentUser]]) {
        [_logoutButton setEnabled:YES];
    } else {
        [_logoutButton setEnabled:NO];
    }
    
    if ([[PFUser currentUser] objectForKey:@"profile"][@"name"]) {
        self.FBUserName.text = [[PFUser currentUser] objectForKey:@"profile"][@"name"];
    }
}

- (IBAction)ButaoLogout:(id)sender {
    [PFUser logOut];
    UIViewController *newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Home"];
    self.slidingViewController.topViewController = newTopViewController;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)revealMenu:(id)sender
{
    [self.slidingViewController anchorTopViewTo:ECRight];
}



#pragma mark - CollectionView Methods
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{

    return self.workArray.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    EDultimasObrasPubCell * cell = (EDultimasObrasPubCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"CellultimasObrasPub" forIndexPath:indexPath];
    
    //[cell.activityIndicator startAnimating];

    

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // Do some stuff off the main queue
       // NSURL * url = [NSURL URLWithString:[workDict objectForKey:@"Ficheiro"]];
       // UIImage *im = [UIImage imageWithData:[NSData dataWithContentsOfURL:url]];
        
        NSDictionary *workDict = [NSDictionary dictionaryWithDictionary:[self.workArray objectAtIndex:indexPath.row]];
        dispatch_async(dispatch_get_main_queue(), ^{
            // Do something with the UI
            NSURL * url = [NSURL URLWithString:[workDict objectForKey:@"Ficheiro"]];
            
            [cell.imagem setImageWithURL:url placeholderImage:[UIImage imageNamed:@"face.png"]];
            //[cell.imagem setImageWithURL:url placeholderImage:[UIImage imageNamed:@"face.png"]];
           // cell.imagem.image = imagem.image;
            [cell.nomeImage setText:[NSString stringWithFormat:@"%@",[workDict objectForKey:@"Nome"]]];
            //[cell.dataCria setText:[NSString stringWithFormat:@"%@", [workDict objectForKey:@"data"]]];
            
            cell.viewToWork.layer.shadowOffset = CGSizeMake(3.0, 3.0);
            cell.viewToWork.layer.shadowColor = [UIColor blackColor].CGColor;
            cell.viewToWork.layer.shadowOpacity = 0.3;
            cell.viewToWork.clipsToBounds = NO;
            
            
            [EDAppSupportTasks makeFBRequestForFbId:[workDict objectForKey:@"Autor"] WithCompletionBlock:^(NSError *erro, NSDictionary *result) {
                if (!erro) {
                    
                    [[self.workArray objectAtIndex:indexPath.row] setObject:[result objectForKey:@"name"] forKey:@"NomeAutor"];
                    cell.dataCria.text = [NSString stringWithFormat:@"%@",[result objectForKey:@"name"]];

                } else {
                    NSLog(@"%@",erro);
                }
            }];
            
            [_activity stopAnimating];
           // [cell.activityIndicator stopAnimating];
        });
    });
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableArray *myArray = [NSMutableArray new];
    for (NSDictionary * dict in self.workArray) {
        IDMPhoto *photo;
        NSURL * url = [NSURL URLWithString:[dict objectForKey:@"Ficheiro"]];
        photo = [IDMPhoto photoWithURL:url];
        photo.caption = [NSString stringWithFormat:@"%@\n\nby %@",[dict objectForKey:@"Nome"],[dict objectForKey:@"NomeAutor"]];
        
        [myArray addObject:photo];
    }
    IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:myArray];
    browser.delegate = self;
    //browser.displayActionButton = NO;
    browser.displayCounterLabel = YES;
    [self presentViewController:browser animated:YES completion:nil];
    [browser setInitialPageIndex:(NSUInteger)indexPath.row];
}

#pragma mark - IDMPhotoBrowser Delegate
- (void)photoBrowser:(IDMPhotoBrowser *)photoBrowser didShowPhotoAtIndex:(NSUInteger)index
{
    [photoBrowser photoAtIndex:index];
}


@end
