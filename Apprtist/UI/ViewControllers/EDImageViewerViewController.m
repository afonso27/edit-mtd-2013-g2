//
//  EDImageViewerViewController.m
//  Apprtist
//
//  Created by Jorge Oliveira on 23/09/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//

#import "EDImageViewerViewController.h"

@interface EDImageViewerViewController ()


@end

@implementation EDImageViewerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.galleryImages = [[NSMutableArray alloc] initWithArray:self.galleryImagesFromOtherVC];
    NSLog(@"%d",[self.galleryImages count]);
    
    self.currentIndex =self.currentIndexFromOtherVC;
    NSLog(@"%d",self.currentIndex);
    


}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




@end
