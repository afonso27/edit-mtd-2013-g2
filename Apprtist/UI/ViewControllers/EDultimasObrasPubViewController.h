//
//  EDultimasObrasPubViewController.h
//  Apprtist
//
//  Created by Afonso Neto on 7/3/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IDMPhotoBrowser.h"

@interface EDultimasObrasPubViewController : UICollectionViewController <IDMPhotoBrowserDelegate>

@property (strong, nonatomic) IBOutlet UICollectionView* collectionView;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activity;

@property (strong, nonatomic) IBOutlet UIButton *logoutButton;
@property (strong, nonatomic) IBOutlet UILabel *FBUserName;
@end
