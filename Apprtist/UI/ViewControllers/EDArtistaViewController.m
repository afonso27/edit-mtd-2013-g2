//
//  EDArtistaViewController.m
//  Apprtist
//
//  Created by Jorge Oliveira on 29/04/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//

#import <Parse/Parse.h>
#import "EDArtistaViewController.h"
#import "EDArtFilesManager.h"
#import "EDVerArtistaCollectionViewCell.h"
#import "EDArtFiles.h"
#import "EDAppSupportTasks.h"

#import "EDRequest.h"
#import "CJSONDeserializer.h"
#import "EDURLConnectionLoader.h"


@implementation EDArtistaViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"ArtistCell"];
}


-(void) viewWillAppear:(BOOL)animated{
    
   /* self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    self.activityIndicator.center = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2);
    self.activityIndicator.frame = self.view.frame;
    self.activityIndicator.hidesWhenStopped = YES;
    self.activityIndicator.backgroundColor = [UIColor blackColor];
    [self.view addSubview:self.activityIndicator];
    [_activityIndicator startAnimating]; */
    
    
    
    EDRequest *pedido = [[EDRequest alloc] init];
    EDURLConnectionLoader *loader = [[EDURLConnectionLoader alloc] initWithRequest:[pedido RequestWorksByWorkIds:@[@"1",@"2"]]] ;
    loader.progressBlock = 0;
    self.author = [NSMutableArray new];
	loader.completionBlock = ^(NSError *error, NSData * responseData){
        NSError *jsonParseError = NULL;
        NSDictionary *json = [[CJSONDeserializer deserializer] deserializeAsDictionary:responseData error:&jsonParseError];
        if (!jsonParseError) {
            // Add tag name to Array
            for (NSDictionary *dict in [json objectForKey:@"Data"]) {
                NSMutableDictionary *works = [NSMutableDictionary new];
                [works setObject:[dict objectForKey:@"Title"] forKey:@"nome"];
                [works setObject:[dict objectForKey:@"AuthorFBID"] forKey:@"autor"];
                [works setObject:[dict objectForKey:@"File"] forKey:@"foto"];
                [works setObject:[dict objectForKey:@"Latitude"] forKey:@"latitude"];
                [works setObject:[dict objectForKey:@"Longitude"] forKey:@"longitude"];
                [self.author addObject:works];
            }

            NSLog(@"%@",self.author);
            [self.collectionView reloadData];
        }
	};
	[loader load];
    // [activityIndicator stopAnimating];
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSLog(@" conta %d", self.author.count );
    return  self.author.count;
    
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString * identifier = @"ArtistCell";
    EDVerArtistaCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // Do some stuff off the main queue
     //   NSURL * url = [NSURL URLWithString:[[self.artistArtwork objectAtIndex:indexPath.row] objectForKey:@"foto"]];
     //   UIImage * image = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:url]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            // Do something with the UI
            
            
         //   cell.obrasArtistaThumb.image = image;
         //   cell.latitudeLbl.text = [NSString stringWithFormat:@"Latitude: %@",[[self.artistArtwork objectAtIndex:indexPath.row] objectForKey:@"latitude"]];
         //   cell.longitudeLbl.text = [NSString stringWithFormat:@"Longitude: %@",[[self.artistArtwork objectAtIndex:indexPath.row] objectForKey:@"longitude"]];
            
         //   [self.activityIndicator stopAnimating];
            
        });
    });
    
    return cell;
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*

#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    // TODO: Select Item
}
- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    // TODO: Deselect item
}*/
@end
