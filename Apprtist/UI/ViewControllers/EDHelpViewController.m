//
//  EDHelpViewController.m
//  Apprtist
//
//  Created by Jorge Oliveira on 13/09/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//

#import "EDHelpViewController.h"
#import "ECSlidingViewController.h"

@interface EDHelpViewController ()

@end

@implementation EDHelpViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Butão do menu
    UIButton * button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 38, 30)];
    [button setImage:[UIImage imageNamed:@"menuButton.png"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(revealMenu:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * menu = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = menu;
    
    self.title = @"Help";

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)revealMenu:(id)sender
{
    [self.slidingViewController anchorTopViewTo:ECRight];
}

@end
