//
//  EDImageViewerViewController.h
//  Apprtist
//
//  Created by Jorge Oliveira on 23/09/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EDImageViewerViewController : UIViewController

@property(nonatomic, retain)NSMutableArray *galleryImages; //Array holding the image file paths
@property(nonatomic,retain) NSMutableArray * galleryImagesFromOtherVC;

@property (nonatomic, assign) NSInteger currentIndex;
@property(nonatomic,assign) NSInteger currentIndexFromOtherVC;


@end
