//
//  EDUnpublishedWorksVC.m
//  Apprtist
//
//  Created by Jorge Oliveira on 03/07/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//

#import "EDUnpublishedWorksVC.h"
#import "EDArtFilesManager.h"
#import "EDArtFiles.h"
#import "EDUnpublishedWorksCVCell.h"
#import "EDPublicarImagemViewController.h"
#import "ECSlidingViewController.h"
#import "EDAppSupportTasks.h"
#import "EDEdicaoViewController.h"


@interface EDUnpublishedWorksVC ()


@end

@implementation EDUnpublishedWorksVC
{
    NSMutableArray * myPhotos;
    BOOL deleteEnabled;
    NSMutableArray * selectedItems;
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    

    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"texture_grain.png"]];
    
    _userUnpublishedArtworks = [NSMutableArray new];
    NSLog(@"%@", [NSString stringWithFormat:@"%@",self.unpublishedAuthor]);
    [[EDArtFilesManager sharedManager] getAllArtworksForUserId:[NSString stringWithFormat:@"%@",self.unpublishedAuthor] withCompletionBlock:^(NSError *error, NSArray *artworks) {
        if(error){
            NSLog(@"Error: %@",error);
        }
        else {
            for (EDArtFiles *artwork in artworks) {
                
                [_userUnpublishedArtworks addObject:artwork.artFileURLLocation];
            }
        }
    }];
    
    myPhotos = [NSMutableArray new];
    myPhotos = [NSMutableArray arrayWithArray:[self loadInternalPhotos]];
    
    NSLog(@"%@",myPhotos);
    
    deleteEnabled = NO;
    
    if ([myPhotos count] == 0) {
        [self.editBtn setEnabled:NO];
    }
    
    selectedItems = [NSMutableArray new];
    
    self.delete = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash target:self action:@selector(deletePhotos)];
    
}

-(NSArray *)loadInternalPhotos
{
    
    NSArray * filePathsArray = [NSArray new];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString * pastaApprtist = [NSString stringWithFormat:@"%@/Apprtist",documentsDirectory];
    filePathsArray = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:pastaApprtist  error:nil];
    
    NSLog(@"%d",[filePathsArray count]);
    
    return filePathsArray;
    
}

-(void)deleteInternalPhotos:(NSString*)imageName
{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString * pastaApprtist = [NSString stringWithFormat:@"%@/Apprtist",documentsDirectory];
  
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    NSArray *fileArray = [fileMgr contentsOfDirectoryAtPath:pastaApprtist error:nil];
    
    for (NSString *filename in fileArray)  {
        
        if ([filename isEqualToString:imageName]) {
            [fileMgr removeItemAtPath:[pastaApprtist stringByAppendingPathComponent:filename] error:NULL];
        }
    }
    
    myPhotos = [NSMutableArray arrayWithArray:[self loadInternalPhotos]];
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    //return [_userUnpublishedArtworks count];
    return [myPhotos count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * identifier = @"CellUnpublished";
    
    EDUnpublishedWorksCVCell * cell = (EDUnpublishedWorksCVCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        
   // NSURL * url = [NSURL URLWithString:[_userUnpublishedArtworks objectAtIndex:indexPath.row]];
    
    UIImage * __block image;
    
    //cell.imageView.layer.cornerRadius = 5;
    //EDArtFiles *objecto = [[EDArtFiles alloc] init];
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // Do some stuff off the main queue
        
        //image = [EDAppSupportTasks getCachedImage:[_userUnpublishedArtworks objectAtIndex:indexPath.row] forObject: objecto];
        
        //image = [EDAppSupportTasks getCachedImage:[NSString stringWithFormat:@"%@",objecto.artID] forObject:objecto];
        
        //image = [UIImage imageWithContentsOfFile:[myPhotos objectAtIndex:indexPath.row]];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString * pastaApprtist = [NSString stringWithFormat:@"%@/Apprtist",documentsDirectory];
        NSString * ficheiro = [NSString stringWithFormat:@"%@/%@",pastaApprtist,[myPhotos objectAtIndex:indexPath.row]];
        
       image = [UIImage imageWithData:[NSData dataWithContentsOfFile:ficheiro]];
        
        cell.viewToWork.layer.shadowOffset = CGSizeMake(3.0, 3.0);
        cell.viewToWork.layer.shadowColor = [UIColor blackColor].CGColor;
        cell.viewToWork.layer.shadowOpacity = 0.3;
        cell.viewToWork.clipsToBounds = NO;
        
        /*cell.checkmarkView.backgroundColor = [UIColor grayColor];
        cell.checkmarkView.alpha = 0.3f;
        cell.checkmarkImage.alpha = 1.0f;
        cell.checkmarkView.hidden = NO;*/
        
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            // Do something with the UI
            cell.imageThumb.image = image;
        });
    });
    
    //cell.backgroundView = [[UIImageView alloc] initWithImage:nil];
    cell.selectedBackgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"photo-selected.png"]];
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    //[self performSegueWithIdentifier:@"editArt" sender:self];

    
    if (deleteEnabled) {
        
        collectionView.allowsMultipleSelection = YES;
        
        [selectedItems addObject:[myPhotos objectAtIndex:indexPath.row]];
        
    }else{
        
        UINavigationController *navigationController = [self.storyboard instantiateViewControllerWithIdentifier:@"Edit"];
        EDEdicaoViewController *edit = navigationController.viewControllers[0];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString * pastaApprtist = [NSString stringWithFormat:@"%@/Apprtist",documentsDirectory];
        NSString * ficheiro = [NSString stringWithFormat:@"%@/%@",pastaApprtist,[myPhotos objectAtIndex:indexPath.row]];
        UIImage * image = [UIImage imageWithContentsOfFile:ficheiro];
        
        [edit openToReedit:image];
        
        [self.navigationController pushViewController:edit animated:NO];

    }
    
}


- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (deleteEnabled) {
        
        collectionView.allowsMultipleSelection = YES;
        
        [selectedItems removeObject:[myPhotos objectAtIndex:indexPath.row]];
        
    }
}



//metodo que reencaminha para ecrã de publicação da obra.

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    
    if([segue.identifier isEqualToString:@"publishSegue"]){
         
        EDUnpublishedWorksCVCell * cell = (EDUnpublishedWorksCVCell *)sender;
        NSIndexPath * indexPath = [self.collectionView indexPathForCell:cell];
        
        EDPublicarImagemViewController *pivc = (EDPublicarImagemViewController *)[segue destinationViewController];
        
        //pivc.img = [_userUnpublishedArtworks objectAtIndex:indexPath.row];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString * pastaApprtist = [NSString stringWithFormat:@"%@/Apprtist",documentsDirectory];
        NSString * ficheiro = [NSString stringWithFormat:@"%@/%@",pastaApprtist,[myPhotos objectAtIndex:indexPath.row]];
        pivc.img = ficheiro;
    }
    
   /*if([segue.identifier isEqualToString:@"editArt"]){
        
        EDUnpublishedWorksCVCell * cell = (EDUnpublishedWorksCVCell *)sender;
        NSIndexPath * indexPath = [self.collectionView indexPathForCell:cell];
        
        EDEdicaoViewController *edit = (EDEdicaoViewController *)[segue destinationViewController];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString * pastaApprtist = [NSString stringWithFormat:@"%@/Apprtist",documentsDirectory];
        NSString * ficheiro = [NSString stringWithFormat:@"%@/%@",pastaApprtist,[myPhotos objectAtIndex:indexPath.row]];
        UIImage * image = [UIImage imageWithContentsOfFile:ficheiro];
        [edit openToReedit:image];
        
    }*/
    
    
}

#pragma mark - Delete Unpublished Works


- (IBAction)deleteArtworks:(id)sender {
    
    if (deleteEnabled) {
        
        deleteEnabled = NO;
        
    }else{
        
        if ([myPhotos count] >0 ) {
            
            deleteEnabled = YES;
            
            [self.delete setEnabled:YES];
            [self.editBtn setEnabled:NO];
            self.navigationItem.rightBarButtonItem = self.delete;
            
        }else{
            [self.editBtn setEnabled:NO];
        }

        
    }
    
}

-(void)deletePhotos
{
    
    if ([selectedItems count] > 0) {
        
        UIAlertView * av = [[UIAlertView alloc] initWithTitle:@"Delete jobs" message:@"Are you sure you want to delete the selected jobs? This operation can't be undone." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Yes", nil];
        av.tag = 0;
        [av show];
        
    }else{
        
        UIAlertView * av =[[UIAlertView alloc] initWithTitle:@"Alert" message:@"No jobs to delete were selected." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [av show];
        av.tag = 3;
        
        deleteEnabled = NO;
        
        if ([myPhotos count] > 0) {
            [self.editBtn setEnabled:YES];
        }else{
            [self.editBtn setEnabled:NO];
        }
        
        [self.delete setEnabled:NO];
        
        self.navigationItem.rightBarButtonItem = self.editBtn;
    }

    
}

-(void)removeThePhotos
{
    
    if ([selectedItems count] > 0) {
        [myPhotos removeObjectsInArray:selectedItems];
        
        for (NSString * photoName in selectedItems) {
            [self deleteInternalPhotos:photoName];
        }
        
        [self.delete setEnabled:NO];
        
        if ([myPhotos count] > 0) {
            [self.editBtn setEnabled:YES];
        }else{
            [self.editBtn setEnabled:NO];
        }
        
        self.navigationItem.rightBarButtonItem = self.editBtn;
        
        deleteEnabled = NO;
        
        [self.collectionView reloadData];
        
        UIAlertView * av = [[UIAlertView alloc] initWithTitle:@"Sucess" message:@"The selected jobs were successfully deleted." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        av.tag = 1;
        [av show];
        
    }
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 0) {
        switch (buttonIndex) {
            case 0:
                
                break;
                
            case 1:
                [self removeThePhotos];
                break;
                
            default:
                break;
        }
    }
}

@end
