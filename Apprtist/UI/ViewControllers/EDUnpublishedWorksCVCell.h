//
//  EDUnpublishedWorksCVCell.h
//  Apprtist
//
//  Created by Jorge Oliveira on 03/07/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EDUnpublishedWorksCVCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIView *viewImage;
@property (weak, nonatomic) IBOutlet UIImageView *imageThumb;
@property (strong, nonatomic) IBOutlet UIView *viewToWork;
@property (strong, nonatomic) IBOutlet UIButton *delete;

@property (strong, nonatomic) IBOutlet UIView *checkmarkView;
@property (strong, nonatomic) IBOutlet UIImageView *checkmarkImage;
@end
