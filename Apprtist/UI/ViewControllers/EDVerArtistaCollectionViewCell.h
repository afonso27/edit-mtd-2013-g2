//
//  EDVerArtistaCollectionViewCell.h
//  Apprtist
//
//  Created by Jorge Oliveira on 01/07/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EDVerArtistaCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *thumbGaleriaArtista;

@end
