//
//  EDVerImagemViewController.m
//  Apprtist
//
//  Created by Jorge Oliveira on 29/04/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//

#import <Parse/Parse.h>
#import "EDVerImagemViewController.h"
#import "ECSlidingViewController.h"
#import "EDArtFilesManager.h"
#import "EDArtistaViewController.h"
#import "EDArtFiles.h"
#import "EDVerArtistaCollectionViewCell.h"
#import "EDAppSupportTasks.h"



@interface EDVerImagemViewController ()


@end

@implementation EDVerImagemViewController
@synthesize imageView,img,fullScreenImageView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad{
    
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"texture_grain.png"]];
    self.imageView.image = self.img;
    UIPinchGestureRecognizer *pinchGesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinchGesture:)];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    tapGesture.numberOfTapsRequired = 1;
    tapGesture.cancelsTouchesInView = NO;
    imageView.userInteractionEnabled = YES;
    [imageView addGestureRecognizer:tapGesture];
    [imageView addGestureRecognizer:pinchGesture];
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        dispatch_async(dispatch_get_main_queue(), ^{
            // Do something with the UI
       /*     [EDAppSupportTasks makeFBRequestForFbId:self.autor WithCompletionBlock:^(NSError *erro, NSMutableDictionary *result) {
                if (!erro) {
                    NSURL * url = [NSURL URLWithString:[result objectForKey:@"foto"]];
                    UIImage * image = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:url]];
                    self.FBProfilePic.image = image;
                    self.FBUserName.text = [NSString stringWithFormat:@"%@",[result objectForKey:@"name"]];
                    if ([[result objectForKey:@"app"] isEqual:@"yes"]) {
                        self.appUser.text = @"Apprtist User";
                    } else {
                        self.appUser.text = @"";
                    }
                } else {
                    NSLog(@"%@",erro);
                }
            }];
            self.artName.text = self.myCurrentSelectedArtName;
            self.obraGeoTag.text = [NSString stringWithFormat:@"%@ (Lat.,Long.)",self.geoTag]; */
            
        });
    });
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)verAutor:(id)sender
{
   [self performSegueWithIdentifier:@"Artista" sender:self];
}

-(void)handleTap:(UIGestureRecognizer *)sender
{
    if (((UITapGestureRecognizer *)sender).view == self.imageView) {
        [self showFloorPlanFullScreen];
    } else {
        [self closeFloorPlanFullScreen];
    }
}

#pragma mark - Gestures
- (void)handlePinchGesture:(id)sender
{
    if (((UIPinchGestureRecognizer *)sender).state == UIGestureRecognizerStateEnded) {
        if(((UIPinchGestureRecognizer *)sender).view == self.imageView)
        {
            if (((UIPinchGestureRecognizer *)sender).scale > 1) {
                [self showFloorPlanFullScreen];
            }
        } else {
            if (((UIPinchGestureRecognizer *)sender).scale < 1) {
                [self closeFloorPlanFullScreen];
            }
        }
    }
}
- (IBAction)handlePinch:(UIPinchGestureRecognizer *)recognizer {
    float imageScale = sqrtf(recognizer.view.transform.a * recognizer.view.transform.a +
                             recognizer.view.transform.c * recognizer.view.transform.c);
    if ((recognizer.scale > 1.0f) && (imageScale >= 4.00)) {
        return;
    }
    if ((recognizer.scale < 1.0f) && (imageScale <= 1.00)) {
        return;
    }
    recognizer.view.transform = CGAffineTransformScale(recognizer.view.transform, recognizer.scale, recognizer.scale);
    recognizer.scale = 1.0;
}

-(IBAction)handlePan:(UIPanGestureRecognizer *)recognizer {
    float imageScale = sqrtf(recognizer.view.transform.a * recognizer.view.transform.a +
                             recognizer.view.transform.c * recognizer.view.transform.c);
    if (imageScale <= 1.00) {
        return;
    }
    CGPoint translation = [recognizer translationInView:self.view];
    recognizer.view.center = CGPointMake(recognizer.view.center.x + translation.x,
                                         recognizer.view.center.y + translation.y);
    [recognizer setTranslation:CGPointMake(0, 0) inView:self.view];
    
}
#pragma mark - FullScreenImage

- (void)showFloorPlanFullScreen{
    
    UIImage *image = self.imageView.image;
    
    [self.view removeGestureRecognizer:self.slidingViewController.panGesture];
    
    //self.view.backgroundColor = [UIColor blackColor];
    
    self.fullScreenImageView = [[UIImageView alloc] initWithImage:image];
    self.fullScreenImageView.frame = self.imageView.frame;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    UIPinchGestureRecognizer *pinchGesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinch:)];
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
    tapGesture.numberOfTapsRequired = 2;
    self.fullScreenImageView.userInteractionEnabled = YES;
    self.fullScreenImageView.multipleTouchEnabled = YES;
    [self.fullScreenImageView addGestureRecognizer:tapGesture];
    [self.fullScreenImageView addGestureRecognizer:panGesture];
    [self.fullScreenImageView addGestureRecognizer:pinchGesture];
    self.fullScreenImageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.view addSubview:fullScreenImageView];
    [UIView animateWithDuration:0.5 animations:^{
        self.fullScreenImageView.backgroundColor = [UIColor blackColor];
        [self.navigationController setNavigationBarHidden:YES animated:YES];
        self.fullScreenImageView.frame = self.view.frame;
    }];
}

- (void)closeFloorPlanFullScreen{
    [UIView animateWithDuration:0.5
                     animations:^{
                        [self.navigationController setNavigationBarHidden:NO animated:YES];
                        self.fullScreenImageView.frame = self.imageView.frame;
                         
                     }
                     completion:^(BOOL finished) {
                         [self.fullScreenImageView removeFromSuperview];
                         self.fullScreenImageView = nil;
                     }];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
        
    if ([segue.identifier isEqual: @"Artista"]){
        
        
        
    }
}



#pragma mark - UIGestureRecognizerDelegate Methods

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}


@end
