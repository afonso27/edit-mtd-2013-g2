//
//  EDMediator.h
//  Apprtist
//
//  Created by Pedro Martins on 5/17/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EDMediator : NSObject
{
   NSString *_str;
}

@property (nonatomic,copy) NSString *str;

+(EDMediator*) sharedMediator;
@end
