//
//  EDArtistasCollectionViewController.h
//  Apprtist
//
//  Created by Jorge Oliveira on 22/05/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EDArtistas.h"

@interface EDArtistasCollectionViewController : UICollectionViewController

-(IBAction)ButaoLogout:(id)sender;
@property(strong,nonatomic) NSMutableArray * artistasUnicos;
@property(strong,nonatomic)NSMutableArray * obrasPorArtista;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;


@property (strong, nonatomic) IBOutlet UILabel *FBUserName;
@property (strong, nonatomic) IBOutlet UIButton *logoutButton;

@property (strong, nonatomic) IBOutlet UIView *navigationBarView;

@end
