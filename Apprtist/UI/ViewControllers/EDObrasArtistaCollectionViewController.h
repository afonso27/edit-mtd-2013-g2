//
//  EDObrasArtistaCollectionViewController.h
//  Apprtist
//
//  Created by Jorge Oliveira on 22/05/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "EDArtistas.h"
#import "IDMPhotoBrowser.h"


@interface EDObrasArtistaCollectionViewController : UICollectionViewController<IDMPhotoBrowserDelegate>

@property(nonatomic, strong) NSArray *myCurrentArtist;
@property(nonatomic, strong) NSString *nomeArtista;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@end
