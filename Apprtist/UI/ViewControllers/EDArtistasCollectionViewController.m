//
//  EDArtistasCollectionViewController.m
//  Apprtist
//
//  Created by Jorge Oliveira on 22/05/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//

#import "EDArtistasCollectionViewController.h"
#import "ECSlidingViewController.h"
#import "EDMenuViewController.h"
#import "EDMenuEdicaoViewController.h"
#import "EDAppDelegate.h"
#import "EDArtistasCollectionViewCell.h"
#import "EDAppSupportTasks.h"
#import "EDObrasArtistaCollectionViewController.h"
#import <Parse/Parse.h>
#import "EDRequest.h"
#import "CJSONDeserializer.h"
#import "EDURLConnectionLoader.h"
//#import <SDWebImage/UIImageView+WebCache.h>

@interface EDArtistasCollectionViewController ()

@property(nonatomic, strong) NSArray * selectedArtist;
@property(nonatomic, strong) NSMutableArray *authorArray;
@property(nonatomic, strong) NSString *nomeDoArtista;
//@property(nonatomic, strong) UIBarButtonItem *logoutButton;
@property (nonatomic, strong) NSArray *temEstaApp;

@end


@implementation EDArtistasCollectionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - View Methods
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [_logoutButton addTarget:self action:@selector(ButaoLogout:) forControlEvents:UIControlEventTouchUpInside];
    
    self.FBUserName.text = [[PFUser currentUser] objectForKey:@"profile"][@"name"];
    
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"texture_grain.png"]];
    //self.logoutButton = [[UIBarButtonItem alloc] initWithTitle:@"Log Out" style:UIBarButtonItemStyleBordered target:self action:@selector(ButaoLogout:)];
    //self.navigationItem.rightBarButtonItem = self.logoutButton;
    
    //[self.logoutButton setEnabled:YES];
    
    // Butão do menu
    UIButton * button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 38, 30)];
    [button setImage:[UIImage imageNamed:@"menuButton.png"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(revealMenu:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * menu = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = menu;
}

-(void) viewWillAppear:(BOOL)animated{
    
    /*self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    self.activityIndicator.center = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2);
    self.activityIndicator.frame = self.view.frame;
    self.activityIndicator.hidesWhenStopped = YES;
    self.activityIndicator.backgroundColor = [UIColor blackColor];
    [self.view addSubview:self.activityIndicator];
    [_activityIndicator startAnimating];*/
    EDRequest *pedido = [[EDRequest alloc] init];
    EDURLConnectionLoader *loader = [[EDURLConnectionLoader alloc] initWithRequest:[pedido RequestAuthors]] ;
    loader.progressBlock = 0;
	loader.completionBlock = ^(NSError *error, NSData * responseData){
        NSError *jsonParseError = NULL;
        NSDictionary *json = [[CJSONDeserializer deserializer] deserializeAsDictionary:responseData error:&jsonParseError];
        if (!jsonParseError) {
            self.authorArray = [NSMutableArray new];
            for (NSDictionary *dict in [json objectForKey:@"Data"]) {
                NSMutableDictionary *autorWorks = [NSMutableDictionary new];
                // Add tag name to Array
                [autorWorks setObject:[dict objectForKey:@"Name"] forKey:@"nome"];
                [autorWorks setObject:[dict objectForKey:@"FBID"] forKey:@"id"];
                [autorWorks setObject:[dict objectForKey:@"Works"] forKey:@"works"];
                [self.authorArray addObject:autorWorks];
            }
            [self.collectionView reloadData];
        }
	};
	[loader load];
}

- (IBAction)ButaoLogout:(id)sender {
    [PFUser logOut];
    [_logoutButton setEnabled:NO];
    UIViewController *newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Home"];
    self.slidingViewController.topViewController = newTopViewController;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)revealMenu:(id)sender
{
    [self.slidingViewController anchorTopViewTo:ECRight];
}


#pragma mark - CollectionView Methods
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.authorArray.count;
    
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * identifier = @"CellArtista";
    
    EDArtistasCollectionViewCell * cell = (EDArtistasCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // Do some stuff off the main queue
        dispatch_async(dispatch_get_main_queue(), ^{
            // Do something with the UI
            
            //cell.layer.cornerRadius = 25;
            
            cell.viewToWork.layer.shadowOffset = CGSizeMake(3.0, 3.0);
            cell.viewToWork.layer.shadowColor = [UIColor blackColor].CGColor;
            cell.viewToWork.layer.shadowOpacity = 0.3;
            cell.viewToWork.clipsToBounds = NO;
            
            
            [EDAppSupportTasks makeFBRequestForFbId:[NSString stringWithFormat:@"%@",[[self.authorArray objectAtIndex:indexPath.row] objectForKey:@"id"]] WithCompletionBlock:^(NSError *error,NSDictionary *result){
                if (!error) {
                    
                    //NSURL * url = [NSURL URLWithString:[result objectForKey:@"foto"]];
                    // UIImage * image = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:url]];
                    
                    NSString * facebookID = [[self.authorArray objectAtIndex:indexPath.row] objectForKey:@"id"];
                    
                    NSURL *pictureURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?width=200&height=200&return_ssl_resources=1", facebookID]];
                    
                    [cell.artistaThumb setImageWithURL:pictureURL placeholderImage:[UIImage imageNamed:@"face.png"]];
                    
                    
                    cell.artworkNumber.text = [NSString stringWithFormat:@"%lu Artworks",(unsigned long)[[[self.authorArray objectAtIndex:indexPath.row] objectForKey:@"works"] count]];
                    
                    cell.nomeLabel.text = [NSString stringWithFormat:@"%@",[result objectForKey:@"name"]];
                    
                    if ([[result objectForKey:@"app"] isEqual:@"yes"]) {
                        cell.apprtistUser.text = @"Apprtist User";
                    } else {
                        cell.apprtistUser.text = @"";
                    }
                    [_activityIndicator stopAnimating];
                } else {
                    NSLog(@"%@",error);
                }
            }];
        });
    });
    return cell;
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    _nomeDoArtista = [NSString stringWithFormat:@"%@",[[self.authorArray objectAtIndex:indexPath.row] objectForKey:@"nome"]];
    _selectedArtist = [[self.authorArray objectAtIndex:indexPath.row] objectForKey:@"works"];
    
    if ( [[[self.authorArray objectAtIndex:indexPath.row] objectForKey:@"works"] count] == 0){
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                        message:@"User hasn't published any artworks."
                                                       delegate:self
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles: nil];
        [alert show];
        
    }else {
        
        [self performSegueWithIdentifier:@"ObrasArtistaSegue" sender:self];
    }
}

#pragma mark - Navigation Method
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"ObrasArtistaSegue"]) {
        
        EDObrasArtistaCollectionViewController * ogcvc = (EDObrasArtistaCollectionViewController *)segue.destinationViewController;
        ogcvc.myCurrentArtist = _selectedArtist;
        ogcvc.nomeArtista = _nomeDoArtista;
        
    }
}

@end
