//
//  EDMenuEdicaoViewController.h
//  Apprtist
//
//  Created by Jorge Oliveira on 29/04/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EDMenuEdicaoViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *toolsPencil;
@property (weak, nonatomic) IBOutlet UIButton *toolsEreaser;
@property (weak, nonatomic) IBOutlet UIButton *toolsCamera;
@property (weak, nonatomic) IBOutlet UIButton *toolsAlbum;
@property (retain) NSString* activeTool;
- (IBAction)handlePencil:(id)sender;
- (IBAction)handlerubber:(id)sender;
- (IBAction)handleCamera:(id)sender;
- (IBAction)handleAlbum:(id)sender;
- (IBAction)handleClear:(id)sender;
- (IBAction)handleSave:(id)sender;
- (IBAction)handlePublish:(id)sender;

@end
