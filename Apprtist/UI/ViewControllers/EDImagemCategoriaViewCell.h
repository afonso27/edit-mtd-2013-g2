//
//  EDImagemCategoriaViewCell.h
//  Apprtist
//
//  Created by Jorge Oliveira on 06/05/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EDImagemCategoriaViewCell : UICollectionViewCell

@property(weak)IBOutlet UIImageView * imageView;

@property (strong, nonatomic) IBOutlet UIView *categoryView;

@property (strong, nonatomic) IBOutlet UILabel *categoryLbl;
@property(weak,nonatomic) IBOutlet UILabel * numberOfImages;

@property (strong, nonatomic) IBOutlet UIView *viewToWork;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@end
