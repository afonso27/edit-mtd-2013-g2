//
//  EDEdicaoViewController.m
//  Apprtist
//
//  Created by Jorge Oliveira on 28/04/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//

#import <Parse/Parse.h>
#import "EDRequest.h"
#import "EDReachability.h"
#import "EDEdicaoViewController.h"
#import "ECSlidingViewController.h"
#import "EDMenuViewController.h"
#import "EDMenuEdicaoViewController.h"
#import "EDMediator.h"
#import <QuartzCore/QuartzCore.h>
#import "EDCameraViewController.h"
#import "EDControlarNavegacao.h"
#import "EDURLConnectionLoader.h"
#import "CJSONDeserializer.h"

static BOOL toolsOpen;
BOOL BooldragStarted;
BOOL firstTime;
BOOL isBackground;
BOOL pressedEdit;

@interface EDEdicaoViewController ()
- (IBAction)handleTools:(id)sender;
@property (strong, nonatomic) UIImageView * writeMaskPathView;
@property (strong, nonatomic) UIImageView * compositeImageView;
@property (strong, nonatomic) UIPopoverController * popover;
@property (strong, nonatomic) EDSmoothedBIView* drawLayerMask;
@property (strong, nonatomic) UILongPressGestureRecognizer *longPressGesture;
@property (strong, nonatomic) UIImageView *linesImageView;

@end

@implementation EDEdicaoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    //self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    //self = [super init];
    if (self) {
        // Custom initialization
    }
    return self;
}


#pragma mark - View Methods
- (void)viewWillDisappear:(BOOL)animated
{
    //NSLog(@"View disappear");
    [super viewWillDisappear:animated];

}

-(void)viewDidDisappear:(BOOL)animated
{
   
}

- (void)viewDidLoad
{
    //NSLog(@"View load");
    [super viewDidLoad];
    

	// Do any additional setup after loading the view.
    
    [_logoutButton addTarget:self action:@selector(ButaoLogout:) forControlEvents:UIControlEventTouchUpInside];
    [self updateFBProfile];
    [_loginBtn addTarget:self action:@selector(facebookLoginPressed:) forControlEvents:UIControlEventTouchUpInside];
    self.activityIndicator.hidden = YES;

    
    // shadowPath, shadowOffset, and rotation is handled by ECSlidingViewController.
    // You just need to set the opacity, radius, and color.
    self.view.layer.shadowOpacity = 0.75f;
    self.view.layer.shadowRadius = 10.0f;
    self.view.layer.shadowColor = [UIColor blackColor].CGColor;
    
    if (![self.slidingViewController.underLeftViewController isKindOfClass:[EDMenuViewController class]]) {
        self.slidingViewController.underLeftViewController  = [self.storyboard instantiateViewControllerWithIdentifier:@"Menu"];
    }
    
    
    if (![self.slidingViewController.underRightViewController isKindOfClass:[EDMenuEdicaoViewController class]]) {
        self.slidingViewController.underRightViewController  = [self.storyboard instantiateViewControllerWithIdentifier:@"MenuEdição"];
        
        
    }
    
    
    UIButton * button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 38, 30)];
    
    
    
    [button setImage:[UIImage imageNamed:@"menuButton.png"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(revealMenu:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * menu = [[UIBarButtonItem alloc] initWithCustomView:button];
    
    self.navigationItem.leftBarButtonItem = menu;

    
    [_revealTools addTarget:self action:@selector(revealTools:) forControlEvents:UIControlEventTouchUpInside];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(action2:)
                                                 name:[[EDMediator sharedMediator] str]
                                               object:nil];
    
    
    firstTime = TRUE;
    isBackground = FALSE;
    
    
    /*if ([PFUser currentUser] && [PFFacebookUtils isLinkedWithUser:[PFUser currentUser]]) {
        
        self.navigationBarView.hidden = NO;
        
    } else {
        [PFUser logOut];
        self.navigationBarView.hidden = YES;

    }*/
    self.littleMan.hidden = NO;
    self.logoutButton.hidden = NO;
    self.FBUserName.hidden = NO;
    self.loginBtn.hidden = YES;
    
    self.tipLabel.hidden = NO;
    
    pressedEdit = NO;
    
    //[self.view removeGestureRecognizer:self.slidingViewController.panGesture];


  
}

-(void)updateFBProfile {
    
    if ([[PFUser currentUser] objectForKey:@"profile"][@"name"]) {
        self.FBUserName.text = [[PFUser currentUser] objectForKey:@"profile"][@"name"];
        
        NSLog(@"NOME %@",self.FBUserName.text);
    }
}

#pragma mark Login
- (IBAction)facebookLoginPressed:(id)sender {
    
    
    // Butao Login do Facebook/Parser
    // Set permissions required from the facebook user account
    NSArray *permissionsArray = @[ @"user_about_me", @"user_birthday", @"user_location"];
    // Login PFUser using facebook
    self.activityIndicator.hidden = NO;
    [PFFacebookUtils logInWithPermissions:permissionsArray block:^(PFUser *user, NSError *error) {
        [_activityIndicator stopAnimating]; // Hide loading indicator
        EDRequest *pedido = [[EDRequest alloc] init];
        EDURLConnectionLoader *loader = [[EDURLConnectionLoader alloc] initWithRequest:[pedido RequestAuth]];
        loader.progressBlock = 0;
        loader.completionBlock = ^(NSError *error, NSData * responseData){
            NSError *jsonParseError = NULL;
            NSDictionary *json = [[CJSONDeserializer deserializer] deserializeAsDictionary:responseData error:&jsonParseError];
            if (!jsonParseError) {
                NSDictionary *data = [json objectForKey:@"Data"][@"Author"];
                [[PFUser currentUser] setObject:[data objectForKey:@"Id"] forKey:@"id"];
                [[PFUser currentUser] saveInBackground];
            }
        };
        [loader load];
        [self updateFBProfile];
        
        [self facebookLoginSuccessfull];
        
        if (!user) {
            if (!error) {
                NSLog(@"Uh oh. The user cancelled the Facebook login.");
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Log In Error" message:@"Uh oh. The user cancelled the Facebook login." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Dismiss", nil];
                [alert show];
            } else {
                NSLog(@"Uh oh. An error occurred: %@", error);
                //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Log In Error" message:[error description] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Dismiss", nil];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Log In Error" message:@"Sry can't connect to Facebook" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Dismiss", nil];
                [alert show];
            }
            
            self.littleMan.hidden = NO;
            self.logoutButton.hidden = NO;
            self.FBUserName.hidden = NO;
            self.loginBtn.hidden = YES;
            self.activityIndicator.hidden = YES;
            
        } else if (user.isNew) {
            NSLog(@"User with facebook signed up and logged in!");
            [_logoutButton setEnabled:YES];
            //self.loggedIn.hidden = NO;
            [FBRequestConnection startForMeWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                if (!error) {
                    // Store the current user's Facebook ID on the user
                    [[PFUser currentUser] setObject:[result objectForKey:@"id"]
                                             forKey:@"fbId"];
                    [[PFUser currentUser] saveInBackground];
                }
            }];
            
            [self facebookLoginSuccessfull];
            
        } else {
            NSLog(@"User with facebook logged in!");
            [_logoutButton setEnabled:YES];
            //self.loggedIn.hidden = NO;
            [self facebookLoginSuccessfull];

            
            
        }
    }];
    
    [_activityIndicator startAnimating];
    
}

-(void)facebookLoginSuccessfull
{
    
    
    [UIView animateWithDuration:0.5f animations:^{
        
        self.littleMan.hidden = NO;
        self.logoutButton.hidden = NO;
        self.FBUserName.hidden = NO;
        self.loginBtn.hidden = YES;
        [self updateFBProfile];
        self.activityIndicator.hidden = YES;

        
    }];
    [UIView commitAnimations];
}

- (IBAction)ButaoLogout:(id)sender {
    [PFUser logOut];
    /*[_logoutButton setEnabled:NO];
    UIViewController *newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Home"];
    self.slidingViewController.topViewController = newTopViewController;*/

    
    [UIView animateWithDuration:0.5f animations:^{
        
        
        self.littleMan.hidden = YES;
        self.logoutButton.hidden = YES;
        self.FBUserName.hidden = YES;
        self.loginBtn.hidden = NO;
        self.activityIndicator.hidden = YES;

        
    }];
    [UIView commitAnimations];
}


-(void)viewWillAppear:(BOOL)animated
{
    [self.view removeGestureRecognizer:self.slidingViewController.panGesture];
    
    if ([PFUser currentUser] && [PFFacebookUtils isLinkedWithUser:[PFUser currentUser]]) {
        
        self.littleMan.hidden = NO;
        self.logoutButton.hidden = NO;
        self.FBUserName.hidden = NO;
        self.loginBtn.hidden = YES;
        [self updateFBProfile];
        self.activityIndicator.hidden = YES;

        
        
    } else {
        [self ButaoLogout:self];
        
    }
    
    // shadowPath, shadowOffset, and rotation is handled by ECSlidingViewController.
    // You just need to set the opacity, radius, and color.
    self.view.layer.shadowOpacity = 0.75f;
    self.view.layer.shadowRadius = 10.0f;
    self.view.layer.shadowColor = [UIColor blackColor].CGColor;
    

    if (![self.slidingViewController.underLeftViewController isKindOfClass:[EDMenuViewController class]]) {
        self.slidingViewController.underLeftViewController  = [self.storyboard instantiateViewControllerWithIdentifier:@"Menu"];
    }
    
    
    if (![self.slidingViewController.underRightViewController isKindOfClass:[EDMenuEdicaoViewController class]]) {
        self.slidingViewController.underRightViewController  = [self.storyboard instantiateViewControllerWithIdentifier:@"MenuEdição"];
        
        
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [self.view removeGestureRecognizer:self.slidingViewController.panGesture];

}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Tools Methods implent

- (IBAction)handleTools:(id)sender {
    if(toolsOpen){
        toolsOpen = FALSE;
        [self.slidingViewController resetTopView];
    } else {
        toolsOpen = TRUE;
        [self.slidingViewController anchorTopViewTo:ECLeft];
    }
}

#pragma mark Save
-(void)saveImgToFile
{
    self.writeMaskPathView.alpha = 0;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Save to?"
                                                    message:@"Save to PhotoAlbum or Internal?"
                                                   delegate:self
                                          cancelButtonTitle:@"Cancel"
                                          otherButtonTitles:@"Photo Album", @"Internal",nil];
    [alert setTag:2]; // tag 1 para o tipo de save
    [alert show];
    
}

#pragma mark Publish
-(void)publishToSocial
{
    [self saveImageToPublish];
    [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"PublicaImagem"] animated:YES];
}

#pragma mark Pencil
-(void)activeToolPencil{
    //NSLog(@"first time %hhd",firstTime);
    if (firstTime) {
        [self activAlert];
        self.tipLabel.text = @"Now let's have some fun!";
        pressedEdit = YES;
        return;
    }
    

    
    _drawLayerMask.delegate = self;
    _drawLayerMask.activewrite = FALSE;

}

#pragma mark Eraser
-(void)activeToolEreaser{
    _drawLayerMask.activewrite = TRUE;
}

#pragma mark Presentation
-(void)activeToolClear{
    self.writeMaskPathView.alpha = 0;
    UIImage *image = [self pb_takeSnapshot];
    IDMPhoto *photo = [IDMPhoto photoWithImage:image];
    NSArray *myArray = @[photo];
    IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:myArray];
    browser.delegate = self;
    [self presentViewController:browser animated:YES completion:nil];
}

#pragma mark - Touch Methods
- (BOOL)longPressEvent:(UILongPressGestureRecognizer *)gesture {
    
    CGAffineTransform transform = CGAffineTransformMakeScale(1.05, 1.05);
    
    if(UIGestureRecognizerStateBegan == gesture.state) {
        BooldragStarted = NO;
        
        _compositeImageView.transform = transform;
        _writeMaskPathView.transform = transform;
        self.linesImageView.transform = transform;
    }
    if(UIGestureRecognizerStateChanged == gesture.state) {
        BooldragStarted = YES;
        CGPoint coords = [gesture locationInView:gesture.view];
        _compositeImageView.center = coords;
        _writeMaskPathView.center = coords;
        _drawLayerMask.center = coords;
        self.linesImageView.center = coords;
        
        // Do dragging stuff here
    }
    
    transform = CGAffineTransformMakeScale(1.0, 1.0);
    
    if(UIGestureRecognizerStateEnded == gesture.state) {
        
        if (BooldragStarted == NO)
        {
            // Do tap stuff here
            //NSLog(@"DO TAP");
            
            
        }
        else
        {
            //NSLog(@"DO TAP ended");
        }
        _compositeImageView.transform = transform;
        _writeMaskPathView.transform = transform;
        self.linesImageView.transform = transform;
    }
    return YES;
    
}

#pragma - UICAMERA METHODS
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo
{
    if (!isBackground) {
        if (image.size.height > 600 && image.size.width > 800) {
            UIImage * imagemMod = [UIImage imageWithImage:image scaledToSize:CGSizeMake(800, 600)];
            [self processAddedImage:imagemMod];
        } else {
            [self processAddedImage:image];
        }
    } else {
        [self.view addSubview:[[UIImageView alloc] initWithImage:image]];
        isBackground = FALSE;
    }
    
    self.tipLabel.hidden = YES;

}

-(void)openToReedit:(UIImage*)image
{
    if (!isBackground) {
        if (image.size.height > 600 && image.size.width > 800) {
            UIImage * imagemMod = [UIImage imageWithImage:image scaledToSize:CGSizeMake(800, 600)];
            [self processAddedImage:imagemMod];
        } else {
            [self processAddedImage:image];
        }
    } else {
        [self.view addSubview:[[UIImageView alloc] initWithImage:image]];
        isBackground = FALSE;
    }
    
    self.tipLabel.hidden = YES;
}

#pragma mark - UIImage Methods

- (UIImage *)pb_takeSnapshot {
    UIGraphicsBeginImageContextWithOptions(self.view.bounds.size, NO, [UIScreen mainScreen].scale);
    
    [self.view drawViewHierarchyInRect:self.view.bounds afterScreenUpdates:YES];
    
    // old style [self.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

-(void)addImageToView:(NSString*)sourcePlace {
    EDCameraViewController * albumPhotos = [[EDCameraViewController alloc] init];
    
    UIImagePickerController* pickerAlbum;
    if([sourcePlace isEqualToString:@"Camera"])
    {
        pickerAlbum = [albumPhotos getCameraSource];
    } else {
        if ([sourcePlace isEqualToString:@"Set Background"]) {
            isBackground = TRUE;
        }
        pickerAlbum = [albumPhotos getRollAlbum];
    }
    
    pickerAlbum.delegate = self;
    self.popover = [[UIPopoverController alloc] initWithContentViewController:pickerAlbum];
    self.popover.delegate = self;
    
    [self.popover presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/2, 500, 500) inView:self.slidingViewController.topViewController.view permittedArrowDirections: UIPopoverArrowDirectionAny animated:YES];
}

- (void) processAddedImage:(UIImage*)imageAdd
{
    CGPoint newCenter = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2);
    
    [_writeMaskPathView removeFromSuperview];
    
    _writeMaskPathView = [[UIImageView alloc] initWithImage:imageAdd];
    _writeMaskPathView.center = newCenter;
    _writeMaskPathView.alpha = .2;
    
    [_drawLayerMask removeFromSuperview];
    
    _drawLayerMask = [[EDSmoothedBIView alloc] initWithFrame:_writeMaskPathView.frame];
    _drawLayerMask.delegate = self;
    
    _compositeImageView = [[UIImageView alloc] initWithImage:imageAdd];
    
    
    self.linesImageView = [[UIImageView alloc] initWithFrame:_compositeImageView.frame];
    
    
    
    self.linesImageView.backgroundColor = [UIColor clearColor];
    self.linesImageView.opaque = NO;
    self.linesImageView.center = newCenter;
    self.linesImageView.alpha = 0.0f;
    [self.view addSubview:_writeMaskPathView];
    
    
    _compositeImageView.center = newCenter;
    [self.view addSubview:_compositeImageView];
    firstTime = FALSE;
    [self.view addSubview:_drawLayerMask];
    [self.view addSubview:self.linesImageView];
    
    
    float oldHeight = _writeMaskPathView.frame.size.height;
    float i_width = _writeMaskPathView.frame.size.width;
    float ratio = i_width / oldHeight;
    
    if (_writeMaskPathView.frame.size.width>self.view.frame.size.width) {
        float newHeight = _writeMaskPathView.frame.size.width / ratio;
        CGRect myRect =CGRectMake(_writeMaskPathView.frame.origin.x, _writeMaskPathView.frame.origin.x, self.view.frame.size.width, newHeight);
        _writeMaskPathView.bounds = myRect;
        _compositeImageView.bounds = myRect;
        _drawLayerMask.bounds = myRect;
        self.linesImageView.bounds = myRect;
    }
    if (_writeMaskPathView.frame.size.height>(self.view.frame.size.height-70)) {
        float newWidth = (self.view.frame.size.height-70) * ratio;
        CGRect myRect =CGRectMake(_writeMaskPathView.frame.origin.x, _writeMaskPathView.frame.origin.x, self.view.frame.size.width, newWidth);
        _writeMaskPathView.bounds = myRect;
        _compositeImageView.bounds = myRect;
        _drawLayerMask.bounds = myRect;
        self.linesImageView.bounds = myRect;
    }
    
    CALayer * nws = [CALayer layer];
    _compositeImageView.layer.mask = nws;
    
    UILongPressGestureRecognizer *longpressGesture =
    [[UILongPressGestureRecognizer alloc]  initWithTarget:self
                                                   action:@selector(longPressEvent:)];
    
    longpressGesture.minimumPressDuration = 1;
    [self.view addGestureRecognizer:longpressGesture];
   

    
}

- (UIImage*)drawBitmap :(UIBezierPath*)renderPath drawContainer:(UIImageView*)container
{
    UIGraphicsBeginImageContextWithOptions(container.bounds.size, NO, 0.0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    [container.image drawAtPoint:CGPointZero];
    
    if (_drawLayerMask.activewrite) {
        [[UIColor whiteColor] setStroke];
        CGContextSetBlendMode(context, kCGBlendModeDestinationOut);
    } else{
       [[UIColor blackColor] setStroke];
    }
    [renderPath stroke];
    container.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return container.image;
}

-(void)saveImageToPublish {
    self.writeMaskPathView.alpha = 0;
    self.tipLabel.text = @"";

    UIImage *img = [self pb_takeSnapshot];
    NSArray *paths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *filePath = [NSString stringWithFormat:@"%@/%@",
                          documentsDirectory,@"imgPublica.png"];
    
    [UIImagePNGRepresentation(img) writeToFile:filePath atomically:YES];
}

#pragma mark EDSmoothedBIview Class Delegate Methods

-(void)drawElement:(UIBezierPath*)linepath
{
    [self drawBitmap:linepath drawContainer:self.linesImageView];
    
    
    UIImage*    backgroundImage = self.linesImageView.image;
    CALayer*    aLayer = [CALayer layer];
    CGFloat nativeWidth = CGImageGetWidth(backgroundImage.CGImage);
    CGFloat nativeHeight = CGImageGetHeight(backgroundImage.CGImage);
    CGRect      startFrame = CGRectMake(0.0, 0.0, nativeWidth, nativeHeight);
    aLayer.contents = (id)backgroundImage.CGImage;
    aLayer.frame = startFrame;
    
    _compositeImageView.layer.mask = aLayer;
}

#pragma mark - Support Methods
-(IBAction)revealMenu:(id)sender
{
    if (self.writeMaskPathView.alpha != 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Are you sure?"
                                                        message:@"All your work will be lost."
                                                       delegate:self
                                              cancelButtonTitle:@"Cancel"
                                              otherButtonTitles:@"Save", @"Ok",nil];
        [alert setTag:1]; // tag 1 para o save
        [alert show];
        
    } else {
        [self.slidingViewController anchorTopViewTo:ECRight];
    }
   
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    UIImage *viewImage = [self pb_takeSnapshot];
    
    switch (alertView.tag) {
        case 0: // aqui vem do picker
            if(![title isEqualToString:@"Cancel"]){
                [self addImageToView:title];
            }else{
                    self.tipLabel.text = @"Please add a picture to start your work.";
            }
            break;
        case 1: // aqui vem da revealmenu
            if ([title isEqualToString:@"Save"]) {
                [self saveImgToFile];
            } else if ([title isEqualToString:@"Cancel"]) {
                [self.slidingViewController resetTopView];
                
            } else if ([title isEqualToString:@"Ok"]) {
                [self.slidingViewController anchorTopViewTo:ECRight];
            }
            break;
        case 2: // aqui vem do save
            if ([title isEqualToString:@"Photo Album"]) {
                //Prepara a imagem para guardar
                //No Album de fotografias
                UIImageWriteToSavedPhotosAlbum(viewImage, nil, nil, nil);
            } else if ([title isEqualToString:@"Internal"]) {
                //Nos ficheiros!
                
                // Reservado para as imagens nao publicadas
                
                NSArray *paths = NSSearchPathForDirectoriesInDomains
                 (NSDocumentDirectory, NSUserDomainMask, YES);
                 NSString *documentsDirectory = [paths objectAtIndex:0];
                 
                NSString *nome = [NSString stringWithFormat:@"%0.0f", [[NSDate date] timeIntervalSince1970]];
                
                [self createDirectory:@"Apprtist" atFilePath:documentsDirectory];
    
                 NSString *filePath = [NSString stringWithFormat:@"%@/Apprtist/%@", documentsDirectory,[[@"img" stringByAppendingString:nome] stringByAppendingString:@".png"]];
                 
                 [UIImageJPEGRepresentation(viewImage, 1.0) writeToFile:filePath atomically:YES];
                 NSLog(@"%@", filePath);
            }
            break;
        default:
            NSLog(@"Ups algo correu mal");
            break;
    }
    
}

-(void)createDirectory:(NSString *)directoryName atFilePath:(NSString *)filePath
{
    NSString *filePathAndDirectory = [filePath stringByAppendingPathComponent:directoryName];
    NSError *error;
    
    if (![[NSFileManager defaultManager] createDirectoryAtPath:filePathAndDirectory
                                   withIntermediateDirectories:NO
                                                    attributes:nil
                                                         error:&error])
    {
        NSLog(@"Create directory error: %@", error);
    }
}



-(void)activAlert{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Image Required"
                                                    message:@"Add one image to start from"
                                                   delegate:self
                                          cancelButtonTitle:@"Cancel"
                                          otherButtonTitles:@"Camera", @"Photo Album",@"Set Background",nil];
    [alert setTag:0]; // tag 0 para o picker
    [alert show];
    

}


- (void)action2:(NSNotification *)toolString{
    
    NSString * comparestr = toolString.object;
    
    if ([kED_Edicao_Pencil isEqualToString:comparestr]) {
        [self activeToolPencil];
    } else if ([kED_Edicao_Rubber isEqualToString:comparestr]){
        [self activeToolEreaser];
    } else if ([kED_Edicao_Camera isEqualToString:comparestr]){
        [self addImageToView:@"Camera"];
    } else if ([kED_Edicao_Album isEqualToString:comparestr]){
        [self addImageToView:@"Do Album"];
    } else if ([kED_Edicao_Clear isEqualToString:comparestr]){
        [self activeToolClear];
    } else if ([kED_Edicao_Save isEqualToString:comparestr]){
        [self.slidingViewController resetTopView];
        [self saveImgToFile];
    } else if ([kED_Edicao_Publish isEqualToString:comparestr]){
        [self publishToSocial];
        

    }
    
    _textlabelEditview.alpha = 0;
    
    [self.slidingViewController resetTopView];
}

#pragma mark - IDMPhotoBrowser Delegate
- (void)photoBrowser:(IDMPhotoBrowser *)photoBrowser didShowPhotoAtIndex:(NSUInteger)index
{
    [photoBrowser photoAtIndex:index];
}


#pragma mark - revel tools

-(IBAction)revealTools:(id)sender
{

    if(self.slidingViewController.underRightShowing)
    {
        [self.slidingViewController resetTopView];
        if (!pressedEdit) {
            self.tipLabel.text = @"Don't forget to select the picture by pressing the Edit menu.";
        }
        
    }else{
        
        [self.slidingViewController anchorTopViewTo:ECLeft];
        self.tipLabel.text = @"Good! Now press the Edit icon to select the picture to use.";

    }

    
}



@end
