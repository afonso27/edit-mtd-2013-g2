//
//  EDGaleriaCollectionViewCell.h
//  Apprtist
//
//  Created by Jorge Oliveira on 30/04/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EDGaleriaCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *thumbGaleria;

@property (strong, nonatomic) IBOutlet UILabel *authorLbl;


@property (strong, nonatomic) IBOutlet UILabel *latitudeLbl;
@property (strong, nonatomic) IBOutlet UILabel *longitudeLbl;
@property (strong, nonatomic) IBOutlet UIView *imageView;
@property (strong, nonatomic) IBOutlet UIView *imageViewView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@end
