//
//  EDImagemCategoriaViewController.h
//  Apprtist
//
//  Created by Jorge Oliveira on 29/04/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EDGaleriaCollectionViewController.h"

@interface EDImagemCategoriaViewController : UICollectionViewController<UICollectionViewDelegate>

@property(strong,nonatomic)UIButton * menuBtn;

- (IBAction)revealMenu:(id)sender;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property(strong,nonatomic) NSMutableArray * categoriasNames;

@property (strong, nonatomic) IBOutlet UILabel *categoryNameLbl;

@property (strong, nonatomic) IBOutlet UIView *categoryView;

@property(strong,nonatomic) NSMutableArray * artCategory;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UIButton *logoutButton;
@property (strong, nonatomic) IBOutlet UILabel *userLbl;
@end
