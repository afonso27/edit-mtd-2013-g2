//
//  EDObrasArtistaCollectionViewController.m
//  Apprtist
//
//  Created by Jorge Oliveira on 22/05/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//

#import "EDObrasArtistaCollectionViewController.h"
#import "ECSlidingViewController.h"
#import "EDMenuViewController.h"
#import "EDArtFiles.h"
#import "EDAppDelegate.h"
#import "EDObrasArtistaCollectionViewCell.h"
#import "EDArtFilesManager.h"
#import "EDAppSupportTasks.h"
#import "EDVerImagemViewController.h"
#import "EDRequest.h"
#import "EDURLConnectionLoader.h"
#import "CJSONDeserializer.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface EDObrasArtistaCollectionViewController ()
@property (nonatomic, strong) NSMutableArray *artistArtwork;


@end

@implementation EDObrasArtistaCollectionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - View
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"texture_grain.png"]];
    self.title = self.nomeArtista;
    
}

-(void)viewWillAppear:(BOOL)animated {
    
    /*self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    self.activityIndicator.center = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2);
    self.activityIndicator.frame = self.view.frame;
    self.activityIndicator.hidesWhenStopped = YES;
    self.activityIndicator.backgroundColor = [UIColor blackColor];
    [self.view addSubview:self.activityIndicator];
    [_activityIndicator startAnimating];*/
    
    EDRequest *pedido = [[EDRequest alloc] init];
    EDURLConnectionLoader *loader = [[EDURLConnectionLoader alloc] initWithRequest:[pedido RequestWorksByWorkIds:self.myCurrentArtist]] ;
    loader.progressBlock = 0;
    self.artistArtwork = [NSMutableArray new];
	loader.completionBlock = ^(NSError *error, NSData * responseData){
        NSError *jsonParseError = NULL;
        NSDictionary *json = [[CJSONDeserializer deserializer] deserializeAsDictionary:responseData error:&jsonParseError];
        if (!jsonParseError) {
            // self.authorArray = [NSMutableArray new];
            for (NSDictionary *dict in [json objectForKey:@"Data"]) {
                NSMutableDictionary *works = [NSMutableDictionary new];
                [works setObject:[dict objectForKey:@"Title"] forKey:@"nome"];
                [works setObject:[dict objectForKey:@"AuthorFBID"] forKey:@"autor"];
                [works setObject:[dict objectForKey:@"File"] forKey:@"foto"];
                [works setObject:[dict objectForKey:@"Latitude"] forKey:@"latitude"];
                [works setObject:[dict objectForKey:@"Longitude"] forKey:@"longitude"];
                [self.artistArtwork addObject:works];
            }
            [self.collectionView reloadData];
            
        }
	};
	[loader load];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)revealMenu:(id)sender
{
    [self.slidingViewController anchorTopViewTo:ECRight];
}

#pragma mark - CollectionView
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    return [self.artistArtwork count];
    
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * identifier = @"ObrasArtistaCell";
    EDObrasArtistaCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    [cell.activityIndicator startAnimating];
    cell.latitudeLbl.text = @"";
    cell.longitudeLbl.text = @"";
    
    
    NSURL * url = [NSURL URLWithString:[[self.artistArtwork objectAtIndex:indexPath.row] objectForKey:@"foto"]];
    
    [cell.obrasArtistaThumb setImageWithURL:url placeholderImage:[UIImage imageNamed:@"face.png"]];
    

    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // Do some stuff off the main queue

        
        dispatch_async(dispatch_get_main_queue(), ^{
            // Do something with the UI
            
            /*cell.latitudeLbl.text = [NSString stringWithFormat:@"Latitude: %@",[[self.artistArtwork objectAtIndex:indexPath.row] objectForKey:@"latitude"]];
            cell.longitudeLbl.text = [NSString stringWithFormat:@"Longitude: %@",[[self.artistArtwork objectAtIndex:indexPath.row] objectForKey:@"longitude"]];*/
            
            cell.artTitle.text = [[self.artistArtwork objectAtIndex:indexPath.row] objectForKey:@"nome"];
            
            cell.viewToWork.layer.shadowOffset = CGSizeMake(3.0, 3.0);
            cell.viewToWork.layer.shadowColor = [UIColor blackColor].CGColor;
            cell.viewToWork.layer.shadowOpacity = 0.3;
            cell.viewToWork.clipsToBounds = NO;
            
            [cell.activityIndicator stopAnimating];
            [self.activityIndicator stopAnimating];
            
        });
    });
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSMutableArray *myArray = [[NSMutableArray alloc] init];
    for (NSDictionary * dict in self.artistArtwork) {
        
        IDMPhoto *photo;
        
        NSURL * url = [NSURL URLWithString:[dict objectForKey:@"foto"]];
        
        photo = [IDMPhoto photoWithURL:url];
        
        photo.caption = [NSString stringWithFormat:@"%@\n\nby %@",[dict objectForKey:@"nome"],self.nomeArtista];
        
        [myArray addObject:photo];
        
    }
    
    // Create and setup browser
    IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:myArray];
    browser.delegate = self;
    
    //browser.displayActionButton = NO;
    browser.displayCounterLabel = YES;
    
    // Show
    [self presentViewController:browser animated:YES completion:nil];
    
    [browser setInitialPageIndex:(NSUInteger)indexPath.row];
    
}

#pragma mark - IDMPhotoBrowser Delegate
- (void)photoBrowser:(IDMPhotoBrowser *)photoBrowser didShowPhotoAtIndex:(NSUInteger)index
{
    [photoBrowser photoAtIndex:index];
}


@end
