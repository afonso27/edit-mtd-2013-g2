//
//  EDNewVerImagemViewController.m
//  Apprtist
//
//  Created by Jorge Oliveira on 20/09/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//

#import "EDNewVerImagemViewController.h"

@interface EDNewVerImagemViewController ()

@end

@implementation EDNewVerImagemViewController

@synthesize galleryImages = galleryImages_;
@synthesize imageHostScrollView = imageHostScrollView_;
@synthesize currentIndex = currentIndex_;

@synthesize prevImage;
@synthesize nxtImage;

@synthesize counterTitle;

@synthesize prevImgView;
@synthesize centerImgView;
@synthesize nextImgView;

#define safeModulo(x,y) ((y + x % y) % y)


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
  
    [self.navigationController setNavigationBarHidden:YES];
    
    [super viewDidLoad];
    
    // setup the scrollview
    // the scrollview holds 3 uiimageviews in a row, to make nice swipe possible
    self.imageHostScrollView.contentSize = CGSizeMake(CGRectGetWidth(self.imageHostScrollView.frame)*3, CGRectGetHeight(self.imageHostScrollView.frame));
    
    self.imageHostScrollView.delegate = self;
    
    CGRect rect = CGRectZero;
    
    rect.size = CGSizeMake(CGRectGetWidth(self.imageHostScrollView.frame), CGRectGetHeight(self.imageHostScrollView.frame));
    
    // add prevView as first in line
    UIImageView *prevView = [[UIImageView alloc] initWithFrame:rect];
    self.prevImgView = prevView;
    
    UIScrollView *scrView = [[UIScrollView alloc] initWithFrame:rect];
    [self.imageHostScrollView addSubview:scrView];
    
    scrView.delegate = self;
    [scrView addSubview:self.prevImgView];
    scrView.minimumZoomScale = 1.0;
    scrView.maximumZoomScale = 2.5;
    self.prevImgView.frame = scrView.bounds;
    
    // add currentView in the middle (center)
    rect.origin.x += CGRectGetWidth(self.imageHostScrollView.frame);
    UIImageView *currentView = [[UIImageView alloc] initWithFrame:rect];
    self.centerImgView = currentView;
    //    [self.imageHostScrollView addSubview:self.centerImgView];
    
    scrView = [[UIScrollView alloc] initWithFrame:rect];
    scrView.delegate = self;
    scrView.minimumZoomScale = 1.0;
    scrView.maximumZoomScale = 2.5;
    [self.imageHostScrollView addSubview:scrView];
    
    [scrView addSubview:self.centerImgView];
    self.centerImgView.frame = scrView.bounds;
    
    // add nextView as third view
    rect.origin.x += CGRectGetWidth(self.imageHostScrollView.frame);
    UIImageView *nextView = [[UIImageView alloc] initWithFrame:rect];
    self.nextImgView = nextView;
    //    [self.imageHostScrollView addSubview:self.nextImgView];
    
    scrView = [[UIScrollView alloc] initWithFrame:rect];
    [self.imageHostScrollView addSubview:scrView];
    scrView.delegate = self;
    scrView.minimumZoomScale = 1.0;
    scrView.maximumZoomScale = 2.5;
    
    [scrView addSubview:self.nextImgView];
    self.nextImgView.frame = scrView.bounds;
    
    // center the scrollview to show the middle view only
    [self.imageHostScrollView setContentOffset:CGPointMake(CGRectGetWidth(self.imageHostScrollView.frame), 0)  animated:NO];
    self.imageHostScrollView.userInteractionEnabled=YES;
    self.imageHostScrollView.pagingEnabled = YES;
    self.imageHostScrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
    
    self.imageHostScrollView.delegate = self;
    
    self.prevImgView.contentMode = UIViewContentModeScaleAspectFit;
    self.centerImgView.contentMode = UIViewContentModeScaleAspectFit;
    self.nextImgView.contentMode = UIViewContentModeScaleAspectFit;
    
    
    
    
    self.galleryImages = [[NSMutableArray alloc] initWithArray:self.galleryImagesFromOtherVC];
    NSLog(@"%d",[self.galleryImages count]);
    
    self.currentIndex =self.currentIndexFromOtherVC;
    NSLog(@"%d",self.currentIndex);
    
   /* self.galleryImages = [[NSMutableArray alloc] init];
    [self.galleryImages insertObject:[[NSBundle mainBundle] pathForResource:@"1" ofType:@"png"] atIndex:0];
    [self.galleryImages insertObject:[[NSBundle mainBundle] pathForResource:@"2" ofType:@"png"] atIndex:1];
    [self.galleryImages insertObject:[[NSBundle mainBundle] pathForResource:@"3" ofType:@"png"] atIndex:2];
    [self.galleryImages insertObject:[[NSBundle mainBundle] pathForResource:@"4" ofType:@"png"] atIndex:3];
    */
    
    
    //self.currentIndex = 0;
    
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    recognizer.numberOfTapsRequired = 2;
    recognizer.delegate = self;
    [self.imageHostScrollView addGestureRecognizer:recognizer];
    
    UITapGestureRecognizer * oneTouch = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleOneTouch:)];
    oneTouch.numberOfTapsRequired = 1;
    oneTouch.delegate = self;
    [self.imageHostScrollView addGestureRecognizer:oneTouch];
    
}

-(void)handleOneTouch:(UIGestureRecognizer*)gesture
{
    
    if ( self.navigationController.navigationBar.hidden == NO) {
        
        [self.navigationController setNavigationBarHidden:YES animated:YES];
        
    }else{
        
        [self.navigationController setNavigationBarHidden:NO animated:YES];
    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark -navigation methods



- (IBAction)nextImage:(id)sender {
    [self setRelativeIndex:1];
}

- (IBAction)prevImage:(id)sender {
    //    [self setRelativeIndex:-1];
    
    NSLog(@"Action taken ");
}

#pragma mark - color button actions-
#pragma mark -page controller action-

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView;  {
    //incase we are zooming the center image view parent
    if (self.centerImgView.superview == scrollView){
        return self.centerImgView;
    }
    
    return nil;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView
{
    UIView *subView = [scrollView.subviews objectAtIndex:0];
    
    CGFloat offsetX = (scrollView.bounds.size.width > scrollView.contentSize.width)?
    (scrollView.bounds.size.width - scrollView.contentSize.width) * 0.5 : 0.0;
    
    CGFloat offsetY = (scrollView.bounds.size.height > scrollView.contentSize.height)?
    (scrollView.bounds.size.height - scrollView.contentSize.height) * 0.5 : 0.0;
    
    subView.center = CGPointMake(scrollView.contentSize.width * 0.5 + offsetX,
                                 scrollView.contentSize.height * 0.5 + offsetY);
}

- (void)scrollViewDidScroll:(UIScrollView *)sender {
    //    CGFloat pageWidth = sender.frame.size.width;
    //    pageNumber_ = floor((sender.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView;{
    CGFloat pageWidth = scrollView.frame.size.width;
    previousPage_ = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)sender {
    CGFloat pageWidth = sender.frame.size.width;
    int page = floor((sender.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    
    //incase we are still in same page, ignore the swipe action
    if(previousPage_ == page) return;
    
    if(sender.contentOffset.x >= sender.frame.size.width) {
        //swipe left, go to next image
        [self setRelativeIndex:1];
        
        // center the scrollview to the center UIImageView
        [self.imageHostScrollView setContentOffset:CGPointMake(CGRectGetWidth(self.imageHostScrollView.frame), 0)  animated:NO];
	}
	else if(sender.contentOffset.x < sender.frame.size.width) {
        //swipe right, go to previous image
        [self setRelativeIndex:-1];
        
        // center the scrollview to the center UIImageView
        [self.imageHostScrollView setContentOffset:CGPointMake(CGRectGetWidth(self.imageHostScrollView.frame), 0)  animated:NO];
	}
    
    UIScrollView *scrollView = (UIScrollView *)self.centerImgView.superview;
    scrollView.zoomScale = 1.0;
}

- (void)handleTap:(UITapGestureRecognizer *)recognizer {
    UIScrollView *scrollView = (UIScrollView*)self.centerImgView.superview;
    float scale = scrollView.zoomScale;
    scale += 1.0;
    if(scale > 2.0) scale = 1.0;
    [scrollView setZoomScale:scale animated:YES];
}

#pragma mark - image loading-

-(UIImage *)imageAtIndex:(NSInteger)inImageIndex;{
    // limit the input to the current number of images, using modulo math
    inImageIndex = safeModulo(inImageIndex, [self totalImages]);
    
    //NSString *filePath = [self.galleryImages objectAtIndex:inImageIndex];
    
	UIImage *image = nil;
    //Otherwise load from the file path
    if (nil == image)
    {
        NSDictionary *workDict = [NSDictionary dictionaryWithDictionary:[self.galleryImages objectAtIndex:inImageIndex]];
        
        NSURL * url = [NSURL URLWithString:[workDict objectForKey:@"Ficheiro"]];
        
        UIImage * imageURL = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:url]];
        
        
		//NSString *imagePath = filePath;
        
    
        image = imageURL;

        
        
		/*if(imagePath){
			if([imagePath isAbsolutePath]){
				image = [UIImage imageWithContentsOfFile:imagePath];
			}
			else{
				image = [UIImage imageNamed:imagePath];
			}
            
            if(nil == image){
				image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imagePath]]];
				
			}
        }*/
    }
    
	return image;
}

#pragma mark -

- (NSInteger)totalImages {
    return [self.galleryImages count];
}
- (NSInteger)currentIndex {
    
    
    return safeModulo(currentIndex_, [self totalImages]);
}

- (void)setCurrentIndex:(NSInteger)inIndex {
    currentIndex_ = inIndex;
    
    
    NSString *title = [NSString stringWithFormat:@"%d of %d",self.currentIndex+1,[self.galleryImages count]];
    self.counterTitle.text = title;
    
    if([galleryImages_ count] > 0){
        self.prevImgView.image   = [self imageAtIndex:[self relativeIndex:-1]];
        self.centerImgView.image = [self imageAtIndex:[self relativeIndex: 0]];
        self.nextImgView.image   = [self imageAtIndex:[self relativeIndex: 1]];
    }
}

- (NSInteger)relativeIndex:(NSInteger)inIndex {
    return safeModulo(([self currentIndex] + inIndex), [self totalImages]);
}

- (void)setRelativeIndex:(NSInteger)inIndex {
    [self setCurrentIndex:self.currentIndex + inIndex];
}


@end
