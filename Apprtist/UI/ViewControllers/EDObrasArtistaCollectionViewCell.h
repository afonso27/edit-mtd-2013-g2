//
//  EDObrasArtistaCollectionViewCell.h
//  Apprtist
//
//  Created by Jorge Oliveira on 22/05/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EDObrasArtistaCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *obrasArtistaThumb;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (weak, nonatomic) IBOutlet UILabel *latitudeLbl;
@property (weak, nonatomic) IBOutlet UILabel *longitudeLbl;
@property (strong, nonatomic) IBOutlet UIView *viewToWork;
@property (strong, nonatomic) IBOutlet UILabel *artTitle;
@end
