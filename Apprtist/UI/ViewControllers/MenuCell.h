//
//  MenuCell.h
//  Apprtist
//
//  Created by Jorge Oliveira on 16/06/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;

@end
