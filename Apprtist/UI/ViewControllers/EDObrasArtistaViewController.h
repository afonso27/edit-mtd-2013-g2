//
//  EDObrasArtistaViewController.h
//  Apprtist
//
//  Created by Jorge Oliveira on 29/04/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EDObrasArtistaViewController : UIViewController

@property(strong,nonatomic)UIButton * menuBtn;
-(IBAction)verImagemArtista:(id)sender;
-(IBAction)verAutor:(id)sender;


@end
