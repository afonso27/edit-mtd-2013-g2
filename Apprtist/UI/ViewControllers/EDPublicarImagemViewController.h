//
//  EDPublicarImagemViewController.h
//  Apprtist
//
//  Created by Afonso Neto on 7/3/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EDTagsPickerTableViewController.h"

@interface EDPublicarImagemViewController : UIViewController <UITextFieldDelegate, TagPickerDelegate>
@property (strong, nonatomic) IBOutlet UIImageView *imagem;
@property (strong, nonatomic) IBOutlet UITextField *imgTitle;
@property (strong, nonatomic) IBOutlet UIProgressView *progressoYEAH;
@property (strong, nonatomic) IBOutlet UILabel *publicarLabel;
- (IBAction)publicaImagem:(id)sender;


@property (strong, nonatomic) NSString *img;

@property (nonatomic, strong) EDTagsPickerTableViewController *tagPicker;
@property (nonatomic, strong) UIPopoverController *tagPickerPopover;
- (IBAction)chooseTagButtonTapped:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *tagsView;
@property (strong, nonatomic) IBOutlet UILabel *tagsTitle;

@end
