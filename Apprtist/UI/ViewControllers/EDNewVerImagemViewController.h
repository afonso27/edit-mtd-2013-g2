//
//  EDNewVerImagemViewController.h
//  Apprtist
//
//  Created by Jorge Oliveira on 20/09/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EDNewVerImagemViewController : UIViewController<UIActionSheetDelegate,UIScrollViewDelegate,UIGestureRecognizerDelegate>

{
@private
    NSMutableArray *galleryImages_;
    NSInteger currentIndex_;
    NSInteger previousPage_;
}
@property (nonatomic, retain)  UIImageView *prevImgView; //reusable Imageview  - always contains the previous image
@property (nonatomic, retain)  UIImageView *centerImgView; //reusable Imageview  - always contains the currently shown image
@property (nonatomic, retain)  UIImageView *nextImgView; //reusable Imageview  - always contains the next image image

@property(nonatomic, retain)NSMutableArray *galleryImages; //Array holding the image file paths
@property(nonatomic,retain) NSMutableArray * galleryImagesFromOtherVC;

//@property(nonatomic, retain)UIScrollView *imageHostScrollView; //UIScrollview to hold the images

@property (retain, nonatomic) IBOutlet UIScrollView *imageHostScrollView;

@property (retain, nonatomic) IBOutlet UIButton *prevImage;
@property (retain, nonatomic) IBOutlet UIButton *nxtImage;

@property (retain, nonatomic) IBOutlet UILabel *counterTitle;//A label to update the title with the count of the image

@property (nonatomic, assign) NSInteger currentIndex;
@property(nonatomic,assign) NSInteger currentIndexFromOtherVC;

//navigation buttons methood...
- (IBAction)nextImage:(id)sender;
- (IBAction)prevImage:(id)sender;

-(void)setCounterTitle:(UILabel *)counterTitle;

#pragma mark - image loading-
//Simple method to load the UIImage, which can be extensible -
-(UIImage *)imageAtIndex:(NSInteger)inImageIndex;

@end
