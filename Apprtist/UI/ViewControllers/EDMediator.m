//
//  EDMediator.m
//  Apprtist
//
//  Created by Pedro Martins on 5/17/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//

#import "EDMediator.h"

static EDMediator* _sharedInstance;

@implementation EDMediator : NSObject


+ (EDMediator*)sharedMediator{
    if(_sharedInstance == nil){
        _sharedInstance = [[EDMediator alloc] init];
    }
    return _sharedInstance;
    
}

- (id)init
{
    if(_sharedInstance){
        return nil;
    }
    self = [super init];
    if (self) {
        _str = @"notificacao";
    }
    return self;
}

@end
