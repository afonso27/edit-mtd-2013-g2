//
//  EDAppDelegate.m
//  Apprtist
//
//  Created by Grupo2 on 24/04/13.
//  Copyright (c) 2013 Grupo2. All rights reserved.
//

#import <Parse/Parse.h>
#import "EDAppDelegate.h"
#import "EDArtFiles.h"
#import "EDArtistas.h"
#import "EDArtFilesManager.h"
#import "EDAppSupportTasks.h"

#import "EDReachability.h"

@implementation EDAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    // Informação do Parse Facebook
    
    /* Parse official do projecto
     [Parse setApplicationId:@"RosxOfiqo6Er8D8BHsRIDasYn0sQwG8THzwP3Lzc"
     clientKey:@"1FyCedgx3FAUixDPexFTObaHjHIQ6uTKQ9ffzGzJ"];
     */
    
    [Parse setApplicationId:@"0hjpc8KrVcSKHP4wNv240OzR92aRdhqYqDPR0rdI"
                  clientKey:@"yma2jmxiyJbSUZoW58dGEGJaKIBrnaTqvtmrSzMu"];
    
    
    
        [PFAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
        [PFFacebookUtils initializeFacebook];
        [PFUser enableAutomaticUser];
    
    /*    } else {
     UIAlertView *alerta = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Error: Internet Down" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Dismiss", nil];
     [alerta show];
     } */
    
    // Inicializa o facebook no Parse e o Analytics
    // Para o Parse suportar users anonimos
    //------------------------------
    
    
    [EDAppSupportTasks sharedInstance];
    [EDAppSupportTasks loadData];
    [self customizeAppearence];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

// ****************************************************************************
// App switching methods to support Facebook Single Sign-On.
// ****************************************************************************
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    return [PFFacebookUtils handleOpenURL:url];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */

        [FBSession.activeSession handleDidBecomeActive];
    
}

- (void)applicationWillTerminate:(UIApplication *)application {
    
    /*
     Called when the application is about to terminate.
     Save data if appropriate.
     See also applicationDidEnterBackground:.
     */
    [FBSession.activeSession close];
}

-(void)customizeAppearence
{
   // [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"top_bar.png"] forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setTintColor:[UIColor blackColor]];
    
    
}

@end
