//
//  EDAppSupportTasks.h
//  Apprtist
//
//  Created by Jorge Oliveira on 18/06/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EDArtFiles.h"

@interface EDAppSupportTasks : NSObject

+ (EDAppSupportTasks *)sharedInstance;

+(void)loadData;

+(void)saveUserData;

+(void)setUserObjectData:(id)object forKey:(NSString*)key;

+ (id) userDataObjectForKey:(NSString*)key;


+(void) cacheImage:(NSString *)ImageURLString forObject:(EDArtFiles*)object;

+(UIImage *)getCachedImage: (NSString *) ImageURLString forObject:(EDArtFiles*)object;
+(NSInteger)IdadeAPartirDaDataDeNascimento:(NSString *)DataDeNascimento;


+(void) makeFbRequestForFriedsWithCompletionBlock:(void (^)(NSError*,NSArray*))completionBlock;
+(void) makeFBRequestForFbId:(NSString *)fbId WithCompletionBlock:(void(^)(NSError*,NSDictionary*))completionBlock;
@end
