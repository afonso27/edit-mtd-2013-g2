//
//  EDAppSupportTasks.m
//  Apprtist
//
//  Created by Jorge Oliveira on 18/06/13.
//  Copyright (c) 2013 EDIT_TrabalhoGrupo. All rights reserved.
//

#import "EDAppSupportTasks.h"
#import "EDAppDelegate.h"
#import "EDArtFiles.h"
#import <Parse/Parse.h>
#import "CJSONDeserializer.h"

#define TMP NSTemporaryDirectory()

static EDAppSupportTasks *sharedInstance = nil;

@implementation EDAppSupportTasks


// Get the shared instance and create it if necessary.
+ (EDAppSupportTasks *)sharedInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}


// We can still have a regular init method, that will get called the first time the Singleton is used.
- (id)init
{
    self = [super init];
    
    if (self) {
        // Work your initialising magic here as you normally would
    }
    
    return self;
}


#pragma mark - Saving and loading data

+(void)loadData
{
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString * cacheDirectory = [paths objectAtIndex:0];
    NSString * path = [cacheDirectory stringByAppendingPathComponent:@"cachedArt.plist"];
    
    NSFileManager * fileManager = [NSFileManager defaultManager];
    
    if(![fileManager fileExistsAtPath:path]){
        
        APPDELEGATE.cachedArt = [NSMutableDictionary new];
        [APPDELEGATE.cachedArt writeToFile:path atomically:YES];
        
    }else{
        
        APPDELEGATE.cachedArt = [NSMutableDictionary dictionaryWithContentsOfFile:path];
    }
}

+(void)saveUserData
{
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory,NSUserDomainMask,YES);
    NSString * cachesDirectory = [paths objectAtIndex:0];
    NSString * path = [cachesDirectory stringByAppendingPathComponent:@"cachedArt.plist"];
    
    [APPDELEGATE.cachedArt writeToFile:path atomically:YES];
}

+(void)setUserObjectData:(id)object forKey:(NSString*)key
{
    [[APPDELEGATE cachedArt] setObject:object forKey:key];
}

+ (id) userDataObjectForKey:(NSString*)key{
    
    return [[APPDELEGATE cachedArt] objectForKey:key];
}

+(void)cacheImage:(NSString *)ImageURLString forObject:(EDArtFiles*)object
{
    NSString *stringURL = ImageURLString;
    NSURL  *url = [NSURL URLWithString:stringURL];
    NSData *urlData = [NSData dataWithContentsOfURL:url];
    
    if ( urlData )
    {
        NSArray       *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        
        NSString  *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory,object.artID];
        [urlData writeToFile:filePath atomically:YES];
    }
}

+ (UIImage *) getCachedImage: (NSString *) ImageURLString forObject:(EDArtFiles*)object
{
    
    NSArray       *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString  *documentsDirectory = [paths objectAtIndex:0];
    
    NSString  *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory,object.artID];
    
    UIImage *image;
    
    // Check for a cached version
    if([[NSFileManager defaultManager] fileExistsAtPath: filePath])
    {
        image = [UIImage imageWithContentsOfFile: filePath]; // this is the cached image
    }
    else
    {
        // get a new one
        [self cacheImage:ImageURLString forObject:object];
        image = [UIImage imageWithContentsOfFile: filePath];
    }
    
    return image;
}

#pragma mark - FbRequests

+(void) makeFBRequestForFbId:(NSString *)fbId WithCompletionBlock:(void(^)(NSError*,NSDictionary*))completionBlock{
    
    FBRequest *requestFbById =  [FBRequest requestWithGraphPath:[NSString stringWithFormat:@"%@",fbId] parameters:@{@"fields":@"id,name,picture"} HTTPMethod:@"GET"];
    
    [requestFbById startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        if (!error) {
            NSDictionary *fbRawInfo = result;
            //NSLog(@"fbRawinfo %@",fbRawInfo);
            NSMutableDictionary *fbInfo = [[NSMutableDictionary alloc] init];
            [fbInfo setObject:[fbRawInfo objectForKey:@"id"] forKey:@"id"];
            [fbInfo setObject:[fbRawInfo objectForKey:@"name"] forKey:@"name"];
            [fbInfo setObject:[fbRawInfo objectForKey:@"picture"][@"data"][@"url"] forKey:@"foto"];
            
            PFQuery *temEstaAppQuery = [PFUser query];
            [temEstaAppQuery whereKey:@"fbId" equalTo:fbId];
            [temEstaAppQuery selectKeys:@[@"fbId"]];
            [temEstaAppQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
                if (!error) {
                    for (PFUser *user in objects) {
                        if ([[fbInfo objectForKey:@"id"] isEqual:[user objectForKey:@"fbId"]]) {
                            [fbInfo setObject:@"yes" forKey:@"app"];
                        }
                    }
                    completionBlock(nil, fbInfo);
                } else {
                    completionBlock(error, nil);
                }}];
        }else if ([[[[error userInfo] objectForKey:@"error"] objectForKey:@"type"]
                   isEqualToString: @"OAuthException"]) { // Since the request failed, we can check if it was due to an invalid session
            NSLog(@"The facebook session was invalidated");
            
            completionBlock(error, nil);
        } else {
            completionBlock(error, nil);
            NSLog(@"Some other error: %@", error);
        }
    }];
}



+(void) makeFbRequestForFriedsWithCompletionBlock:(void (^)(NSError*,NSArray*))completionBlock{
    
    //faz a resquest ao facebook
    FBRequest *AmigosComAppRequest =  [FBRequest requestWithGraphPath:@"me/friends" parameters:@{@"fields":@"id,name,installed,picture"} HTTPMethod:@"GET"];
    [AmigosComAppRequest startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        // handle response
        // NSLog(@"fez o request ao facebook");
        if (!error) {
            // Parse the data received
            NSArray *friendObjects = [result objectForKey:@"data"];
            NSMutableArray *amigosComAppId = [[NSMutableArray alloc] init];
            for (NSDictionary *friendObject in friendObjects) {
                if ([friendObject objectForKey:@"installed"]){
                    [amigosComAppId addObject:[friendObject objectForKey:@"id"]];
                }
            }
            NSLog(@"%@", amigosComAppId);
            // Faz o query ao parse
            PFQuery *temEstaAppQuery = [PFUser query];
            [temEstaAppQuery whereKey:@"fbId" containedIn:amigosComAppId];
            [temEstaAppQuery selectKeys:@[@"fbId"]];
            [temEstaAppQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
                if (!error) {
                    NSMutableArray *amigosComApp = [[NSMutableArray alloc] init];
                    for (NSDictionary *friendObject in friendObjects) {
                        if ([friendObject objectForKey:@"installed"]){
                            // Cria o dicionario com a informação de cada amigo que tenha a app EDIT-MTD instalada
                            NSMutableDictionary *amigoComApp = [[NSMutableDictionary alloc] init];
                            [amigoComApp setObject:[friendObject objectForKey:@"id"] forKey:@"id"];
                            [amigoComApp setObject:[friendObject objectForKey:@"name"] forKey:@"nome"];
                            [amigoComApp setObject:[friendObject objectForKey:@"picture"][@"data"][@"url"] forKey:@"foto"];
                            for (PFUser *user in objects) {
                                if ([[amigoComApp objectForKey:@"id"] isEqual:[user objectForKey:@"fbId"]]) {
                                    //NSLog(@"O utilizador %@, está a usar a nossa app", [user objectForKey:@"fbId"]);
                                    [amigoComApp setObject:@"yes" forKey:@"app"];
                                }
                            }
                            // Cria o array de dicionarios de amigos que tenham a app instalada
                            [amigosComApp addObject:amigoComApp];
                        }
                    }
                    //NSLog(@"amigos %@",amigosComApp);
                    completionBlock(nil,amigosComApp);
                } else {
                    NSLog(@"%@", error.localizedDescription);
                }
            }];
        }
        else if ([[[[error userInfo] objectForKey:@"error"] objectForKey:@"type"]
                  isEqualToString: @"OAuthException"]) { // Since the request failed, we can check if it was due to an invalid session
            NSLog(@"The facebook session was invalidated");
            
            completionBlock(error, nil);
        } else {
            completionBlock(error, nil);
            NSLog(@"Some other error: %@", error);
        }
    }];
    
}


+(NSInteger)IdadeAPartirDaDataDeNascimento:(NSString *)DataDeNascimento {
    
    NSDate *DataHoje = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    int time = [DataHoje timeIntervalSinceDate:[dateFormatter dateFromString:DataDeNascimento]];
    int allDays = (((time/60)/60)/24);
    int days = allDays%365;
    int idade = (allDays-days)/365;
    
    if (idade > 17) {
        NSLog(@"é maior de idade");
    } else {
        NSLog(@"é menor de idade");
    }
    return idade;
}

@end
